//
//  CustomTableViewCell.h
//  scrobleTableview
//
//  Created by ceaselez on 08/12/17.
//  Copyright © 2017 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *header1;
@property (weak, nonatomic) IBOutlet UITextField *header2;
@property (weak, nonatomic) IBOutlet UITextField *header3;
@property (weak, nonatomic) IBOutlet UITextField *header4;
@property (weak, nonatomic) IBOutlet UITextField *header5;
@property (weak, nonatomic) IBOutlet UITextField *header6;
@property (weak, nonatomic) IBOutlet UITextField *header8;
@property (weak, nonatomic) IBOutlet UITextField *header7;
@property (weak, nonatomic) IBOutlet UITextField *header9;
@property (weak, nonatomic) IBOutlet UITextField *data1;
@property (weak, nonatomic) IBOutlet UITextField *data2;
@property (weak, nonatomic) IBOutlet UITextField *data3;
@property (weak, nonatomic) IBOutlet UITextField *data4;
@property (weak, nonatomic) IBOutlet UITextField *data5;
@property (weak, nonatomic) IBOutlet UITextField *data6;
@property (weak, nonatomic) IBOutlet UITextField *data7;
@property (weak, nonatomic) IBOutlet UITextField *data8;
@property (weak, nonatomic) IBOutlet UITextField *data9;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *dataWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headerWidth;

@end
