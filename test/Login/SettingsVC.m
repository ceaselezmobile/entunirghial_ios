

#import "SettingsVC.h"

@interface SettingsVC ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *imagearray,*textarray;
}

@end

@implementation SettingsVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    imagearray=[[NSMutableArray alloc]initWithObjects:@"About.png",@"feedback.png",@"Privacy.png",@"email.png",@"T&C.png", nil];
    textarray=[[NSMutableArray alloc]initWithObjects:@"About Us",@"Feedback",@"Privacy Policy",@"Change Password",@"Terms & Conditions", nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
- (IBAction)Backbtn:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
    }
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 64;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    UIImageView *img=(UIImageView*)[cell viewWithTag:1];
    UILabel *txtlbl=(UILabel*)[cell viewWithTag:2];
    txtlbl.text=[textarray objectAtIndex:indexPath.row];
    img.image=[UIImage imageNamed:[imagearray objectAtIndex:indexPath.row]];
    img.image = [img.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [img setTintColor:[UIColor colorWithRed:0.49 green:0.34 blue:0.76 alpha:1.0]];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row==0||indexPath.row==2||indexPath.row==4)
    {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            self.AboutVC = [storyBoard instantiateViewControllerWithIdentifier:@"AboutVC"];
        self.AboutVC.str=[textarray objectAtIndex:indexPath.row];
        [self presentViewController:_AboutVC animated:YES completion:nil];
    }
    else if(indexPath.row==3)
    {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            self.UpdatePassVC = [storyBoard instantiateViewControllerWithIdentifier:@"UpdatePassVC"];
        [self presentViewController:_UpdatePassVC animated:YES completion:nil];
    }
    else if(indexPath.row==1)
    {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        self.FeedbackViewController = [storyBoard instantiateViewControllerWithIdentifier:@"FeedbackViewController"];
        [self presentViewController:_FeedbackViewController animated:YES completion:nil];
    }
}
@end
