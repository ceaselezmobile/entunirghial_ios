

#ifndef GlobalURL_h
#define GlobalURL_h
#define BASEURL @"http://gadltechrefresh.in/services/MITREnterprize.svc/"
//#define BASEURL @"http://corptest.entunir.com/services/MITREnterprize.svc/"

#define Login @"MITRCorpLogin"
#define GetExternalStakeHolder @"GetExternalStakeHolder"
#define PostExtActionPlannerDetails @"PostExtActionPlannerDetails"
#define PostMITRActionPlannerDetails @"PostMITRActionPlannerDetails"
#define ActionPlannerList @"ActionPlannerList"
#define PostCreateAdhocMeeting @"PostCreateAdhocMeeting"
#define PostExtStakeHolderContactDetails @"PostExtStakeHolderContactDetails"
#define PostUpdateExtStakeHolderContactDetails @"PostUpdateExtStakeHolderContactDetails"
#define PostActionStatusUpdate @"PostActionStatusUpdate"
#define SendActionStatusCompleteMail @"SendActionStatusCompleteMail"
#define PostDeleteExtStakeHolder @"PostDeleteExtStakeHolder"
#define PostUpdateMeetingActionProgress @"PostUpdateMeetingActionProgress"
#define PostDeleteActionPlanner @"PostDeleteActionPlanner"
#define PostUpdateActionPlanner @"PostUpdateActionPlanner"
#define PostDeleteAdhocMeeting @"PostDeleteAdhocMeetingByMeetingDateAndMeetingtypeid"
#define PostAddAdhocMeetingMembers @"PostAddAdhocMeetingMembers"
#define PostAddAdhocMeetingAgendas @"PostAddAdhocMeetingAgendas"
#define PostUpdateAdhocMeetingDiscussion @"PostUpdateAdhocMeetingDiscussion"
#define PostAddAdhocMeetingActions @"PostAddAdhocMeetingActions"
#define PostDisapproveActionProgress @"PostDisapproveActionProgress"
#define POSTSavePDF @"POSTSavePDF"
#define POSTFinishMeeting @"POSTFinishMeeting"
#define POSTUpdateAdhocMeeting @"POSTUpdateAdhocMeeting"
#define PostDeleteAdhocMeetingActions @"PostDeleteAdhocMeetingActions"
#define POSTTestmultipart @"POSTTestmultipart"
#define PostForgotPasswordResetAndSend @"PostForgotPasswordResetAndSend"
#define PostChangePassword @"PostChangePassword"
#define MITRStakeholderMainDetailsByExtId @"MITRStakeholderMainDetailsByExtId"

#define HEADERSFIELD @"Authorization"

#define APICONTENTTYPE @"application/json"
#define APICONTENTGET @"application/x-www-form-urlencoded"


#define NOInternetTitle @"No Connectivity"
#define NOInternetMessage @"Check internet connection"
#endif /* GlobalURL_h */
