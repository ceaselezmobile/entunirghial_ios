//
//  ServiceManager.h
//  D3P
//
//  Created by ceazeles on 18/07/18.
//  Copyright © 2018 ceazeles. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFHTTPSessionManager.h"
#import "AFHTTPRequestOperationManager.h"
#import "AFHTTPRequestOperation.h"
#import "GlobalURL.h"

typedef void (^ResponseBlock)(id responseObject , NSError *error);

@interface ServiceManager : UIViewController


+(ServiceManager *)sharedInstance;
-(void) postwithParameter:(NSDictionary *)params  withUrl:(NSString *)url withHandler:(ResponseBlock)handler;

-(void) getWithParameter:(NSDictionary *)params  withUrl:(NSString *)url withHandler:(ResponseBlock)handler;

@end
