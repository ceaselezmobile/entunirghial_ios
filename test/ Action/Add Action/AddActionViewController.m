#import "AddActionViewController.h"
#import "CustomTableViewCell.h"
#import "SearchViewController.h"
#import "MySharedManager.h"
#import "THDatePickerViewController.h"
#import "Utitlity.h"
#import "Reachability.h"
#import "WebServices.h"
#import "GlobalURL.h"
#import "MBProgressHUD.h"
#import "SWRevealViewController.h"
#import "UIView+Toast.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "AFHTTPSessionManager.h"
#import "AFHTTPRequestOperationManager.h"
#import "AFHTTPRequestOperation.h"
#import <AssetsLibrary/ALAsset.h>
#import <AssetsLibrary/ALAssetRepresentation.h>
//#import "APIManager.h"

@interface AddActionViewController ()<UITextFieldDelegate,UITextViewDelegate,UIDocumentPickerDelegate,THDatePickerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIWebViewDelegate>


@end

@implementation AddActionViewController
{
    __weak IBOutlet UILabel *tittleLabel;
    IBOutlet UITableView *dataTableView;
    MySharedManager *sharedManager;
    NSString *StakeholderCategory;
    NSString *Company;
    NSString *AssignedTo;
    THDatePickerViewController * datePicker2;
    NSDate * curDate;
    THDatePickerViewController * datePicker1;
    NSDate * curDate1;
    NSDateFormatter * formatter;
    int employeeType;
    int stakeHType;
    BOOL dateGap;
    BOOL clearData;
    BOOL submitButtonClicked;
    
    NSString *actionForString;
    NSString *ESHString;
    NSString *stakeholderCategoryString;
    NSString *companyString;
    NSString *assignedToString;
    NSString *startDateString;
    NSString *targetDateCategoryString;
    NSString *actionTypeString;
    NSString *textViewText;
    NSString *fileDescriptionTextViewText;
    NSString *mainCategoryString;
    NSString *mainCategoryID;
    NSString *groupCategoryString;
    NSString *groupCategoryID;
    
    NSString *ActionplannerID;
    UILabel *Docname;
    UIButton *deletebtn,*Downloadbtn,*uploadbtn;
    NSString *alertMessage;
    NSURL *filepath;
    NSData *fileData;
    NSMutableArray *docArray;
    NSMutableArray *docDeletedArray;
    UIImagePickerController *idPicker;
    NSMutableArray *fileAttachment;
    
    __weak IBOutlet UIView *popOverView;
    __weak IBOutlet UIWebView *webView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    popOverView.hidden = YES;
    docArray = [[NSMutableArray alloc] init];
    docDeletedArray = [[NSMutableArray alloc] init];
    fileAttachment = [[NSMutableArray alloc] init];
    
    idPicker = [[UIImagePickerController alloc] init];
    idPicker.delegate = self;
    dataTableView.rowHeight = UITableViewAutomaticDimension;
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    sharedManager = [MySharedManager sharedManager];
    curDate = [NSDate date];
    curDate1 = [NSDate date];
    employeeType = 0;
    [self clearTF];
    actionForString = @"Self";
    
    
    if (_meetingAction) {
        employeeType = 1;
        actionForString = @"Other";
        //        [self clearTF];
    }
    formatter = [[NSDateFormatter alloc] init];
    if (_update) {
        employeeType = 4;
        
        tittleLabel.text = @"Update Action";
        if (_meetingAction) {
            [self loadDataFromMeetingDict];
        }
        else{
            [self loadDataFromApi];
        }
    }
}
-(void) viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    submitButtonClicked = NO;
    if (clearData) {
        [self clearTF];
    }
    [self fillTheTF];
    
}
-(void)viewDidAppear:(BOOL)animated{
    if ([sharedManager.slideMenuSlected isEqualToString:@"yes"]) {
        sharedManager.slideMenuSlected = @"no";
    }
}
- (IBAction)targetDate:(id)sender {
    dateGap = YES;
    
    if(!datePicker1)
        datePicker1 = [THDatePickerViewController datePicker];
    datePicker1.date = curDate1;
    datePicker1.delegate = self;
    [datePicker1 setAllowClearDate:NO];
    [datePicker1 setClearAsToday:YES];
    [datePicker1 setAutoCloseOnSelectDate:NO];
    [datePicker1 setAllowSelectionOfSelectedDate:YES];
    [datePicker1 setDisableYearSwitch:YES];
    
    [datePicker1 setSelectedBackgroundColor:[UIColor colorWithRed:125/255.0 green:208/255.0 blue:0/255.0 alpha:1.0]];
    [datePicker1 setCurrentDateColor:[UIColor colorWithRed:242/255.0 green:121/255.0 blue:53/255.0 alpha:1.0]];
    [datePicker1 setCurrentDateColorSelected:[UIColor yellowColor]];
    
    [datePicker1 setDateHasItemsCallback:^BOOL(NSDate *date) {
        int tmp = (arc4random() % 30)+1;
        return (tmp % 5 == 0);
    }];
    [self presentSemiViewController:datePicker1 withOptions:@{
                                                              KNSemiModalOptionKeys.pushParentBack    : @(NO),
                                                              KNSemiModalOptionKeys.animationDuration : @(0.2),
                                                              KNSemiModalOptionKeys.shadowOpacity     : @(0.2),
                                                              }];
}
- (IBAction)btn:(id)sender {
    if (!_Dependency) {
        if(!datePicker2)
            datePicker2 = [THDatePickerViewController datePicker];
        datePicker2.date = curDate;
        datePicker2.delegate = self;
        [datePicker2 setAllowClearDate:NO];
        [datePicker2 setClearAsToday:YES];
        [datePicker2 setAutoCloseOnSelectDate:NO];
        [datePicker2 setAllowSelectionOfSelectedDate:YES];
        [datePicker2 setDisableYearSwitch:YES];
        
        [datePicker2 setSelectedBackgroundColor:[UIColor colorWithRed:125/255.0 green:208/255.0 blue:0/255.0 alpha:1.0]];
        [datePicker2 setCurrentDateColor:[UIColor colorWithRed:242/255.0 green:121/255.0 blue:53/255.0 alpha:1.0]];
        [datePicker2 setCurrentDateColorSelected:[UIColor yellowColor]];
        
        [datePicker2 setDateHasItemsCallback:^BOOL(NSDate *date) {
            int tmp = (arc4random() % 30)+1;
            return (tmp % 5 == 0);
        }];
        [self presentSemiViewController:datePicker2 withOptions:@{
                                                                  KNSemiModalOptionKeys.pushParentBack    : @(NO),
                                                                  KNSemiModalOptionKeys.animationDuration : @(0.2),
                                                                  KNSemiModalOptionKeys.shadowOpacity     : @(0.2),
                                                                  }];
    }
}

- (void)datePickerDonePressed:(THDatePickerViewController *)datePicker {
    if (datePicker == datePicker2) {
        curDate = datePicker2.date;
        
        NSDate *startDate = curDate1;
        NSDate *endDate = curDate;
        NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitDay
                                                            fromDate:startDate
                                                              toDate:endDate
                                                             options:0];
        if (components.day <=0 || !(dateGap)) {
            [formatter setDateFormat:@"dd-MM-yyyy"];
            startDateString= [formatter stringFromDate:curDate];
        }
        else{
            [self showMsgAlert:@"Enter the proper time duration"];
        }
    }
    else{
        curDate1 = datePicker1.date;
        
        NSDate *startDate = curDate;
        NSDate *endDate = curDate1;
        NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitDay
                                                            fromDate:startDate
                                                              toDate:endDate
                                                             options:0];
        if (components.day >=0) {
            [formatter setDateFormat:@"dd-MM-yyyy"];
            targetDateCategoryString = [formatter stringFromDate:curDate1];
        }
        else{
            [self showMsgAlert:@"Enter the proper time duration"];
        }
    }
    [dataTableView reloadData];
    [self dismissSemiModalView];
}

- (void)datePickerCancelPressed:(THDatePickerViewController *)datePicker {
    [self dismissSemiModalView];
}

- (void)datePicker:(THDatePickerViewController *)datePicker selectedDate:(NSDate *)selectedDate {
    NSLog(@"Date selected: %@",[formatter stringFromDate:selectedDate]);
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (employeeType == 0 ) {
        if (_meetingAction) {
            return 5;
        }
        return 10+docArray.count;
    }
    else if (employeeType == 1) {
        if (_meetingAction) {
            if(stakeHType==1)
                return 7;
            else
                return 9;
        }
        else if(stakeHType==1)
            return 12+docArray.count;
        else
            return 14+docArray.count;
    }
    else
        return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CustomTableViewCell *cell;
    
    if (employeeType == 0) {
        if (_meetingAction) {
            {
                if (indexPath.row == 0) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Employee Type" forIndexPath:indexPath];
                    cell.employeeIV.hidden =YES;
                    cell.employeeTypeTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.employeeTypeTF.text = actionForString;
                    
                }
                else if (indexPath.row == 1) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Target Date" forIndexPath:indexPath];
                    cell.targetDateTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.targetDateTF.text = targetDateCategoryString;
                    cell.targetDateIV.hidden =YES;
                }
                else if (indexPath.row == 2) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Action Type" forIndexPath:indexPath];
                    cell.actionTypeTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.actionTypeTF.text = actionTypeString;
                    cell.actionTypeIV.hidden =YES;
                }
                else if (indexPath.row == 3) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Action Description" forIndexPath:indexPath];
                    cell.actionDescriptionTV.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    [cell.actionDescriptionTV setText:textViewText];
                    cell.actionDescrptionIV.hidden =YES;
                }
                else if (indexPath.row == 4) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"submit" forIndexPath:indexPath];
                    if (_update) {
                        [cell.submitBtn setTitle:@"Update" forState:UIControlStateNormal];
                    }
                    else{
                        [cell.submitBtn setTitle:@"Submit" forState:UIControlStateNormal];
                    }
                }
            }
        }
        else{
            
            if (indexPath.row == 0) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"Category1" forIndexPath:indexPath];
                cell.mainCategoryTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                cell.mainCategoryTF.text = mainCategoryString;
                //                if(_update){
                //                    cell.userInteractionEnabled = NO;
                //                }
            }
            else if (indexPath.row == 1) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"GroupCategory" forIndexPath:indexPath];
                cell.groupCategory.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                cell.groupCategory.text = groupCategoryString;
                //                if(_update){
                //                    cell.userInteractionEnabled = NO;
                //                }
            }
            else if (indexPath.row == 2) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"Employee Type" forIndexPath:indexPath];
                cell.employeeIV.hidden =YES;
                cell.employeeTypeTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                cell.employeeTypeTF.text = actionForString;
                
            }
            else if (indexPath.row == 3) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"Start Date" forIndexPath:indexPath];
                cell.startDate.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                cell.startDate.text = startDateString;
                cell.startDateIV.hidden =YES;
            }
            else if (indexPath.row == 4) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"Target Date" forIndexPath:indexPath];
                cell.targetDateTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                cell.targetDateTF.text = targetDateCategoryString;
                cell.targetDateIV.hidden =YES;
            }
            else if (indexPath.row == 5) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"Action Type" forIndexPath:indexPath];
                cell.actionTypeTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                cell.actionTypeTF.text = actionTypeString;
                cell.actionTypeIV.hidden =YES;
            }
            else if (indexPath.row == 6) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"Action Description" forIndexPath:indexPath];
                cell.actionDescriptionTV.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                [cell.actionDescriptionTV setText:textViewText];
                cell.actionDescrptionIV.hidden =YES;
            }
            else if (indexPath.row == 7) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"File Description" forIndexPath:indexPath];
                cell.fileDescription.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                [cell.fileDescription setText:fileDescriptionTextViewText];
            }
            else if (indexPath.row == 8) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"Document" forIndexPath:indexPath];
            }
            else if (indexPath.row > 8 && indexPath.row != 9+docArray.count) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"doc" forIndexPath:indexPath];
                cell.docName.text = [[docArray objectAtIndex: indexPath.row-9 ] objectForKey:@"FileName"];
                if ([[[docArray objectAtIndex: indexPath.row-9 ] objectForKey:@"ActionplannerID"] isEqualToString:@""]) {
                    cell.downloadBtn.hidden = YES;
                }
                else{
                    cell.downloadBtn.hidden = NO;
                }
            }
            else  {
                cell = [tableView dequeueReusableCellWithIdentifier:@"submit" forIndexPath:indexPath];
                if (_update) {
                    [cell.submitBtn setTitle:@"Update" forState:UIControlStateNormal];
                }
                else{
                    [cell.submitBtn setTitle:@"Submit" forState:UIControlStateNormal];
                }
            }
        }
    }
    else{
        if (_meetingAction) {
            if(stakeHType==1)
            {
                if (indexPath.row == 0) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Employee Type" forIndexPath:indexPath];
                    cell.employeeIV.hidden =YES;
                    cell.employeeTypeTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.employeeTypeTF.text = actionForString;
                    
                }
                else if (indexPath.row == 1) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"StakeHolder Type" forIndexPath:indexPath];
                    cell.StakeHolderTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.StakeHolderTF.text = ESHString;
                    cell.StakeHolderIV.hidden =YES;
                }
                else if (indexPath.row == 2) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Assigned To" forIndexPath:indexPath];
                    cell.assignedToTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.assignedToTF.text = assignedToString;
                    cell.assignedToIV.hidden =YES;
                }
                
                else if (indexPath.row == 3) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Target Date" forIndexPath:indexPath];
                    cell.targetDateTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.targetDateTF.text = targetDateCategoryString;
                    cell.targetDateIV.hidden =YES;
                }
                else if (indexPath.row == 4) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Action Type" forIndexPath:indexPath];
                    cell.actionTypeTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.actionTypeTF.text = actionTypeString;
                    cell.actionTypeIV.hidden =YES;
                }
                else if (indexPath.row == 5) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Action Description" forIndexPath:indexPath];
                    cell.actionDescriptionTV.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    [cell.actionDescriptionTV setText:textViewText];
                    cell.actionDescrptionIV.hidden =YES;
                }
                else if (indexPath.row == 6) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"submit" forIndexPath:indexPath];
                    if (_update) {
                        [cell.submitBtn setTitle:@"Update" forState:UIControlStateNormal];
                    }
                    else{
                        [cell.submitBtn setTitle:@"Submit" forState:UIControlStateNormal];
                    }
                }
            }
            else
            {
                if (indexPath.row == 0) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Employee Type" forIndexPath:indexPath];
                    cell.employeeIV.hidden =YES;
                    cell.employeeTypeTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.employeeTypeTF.text = actionForString;
                    
                }
                else if (indexPath.row == 1) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"StakeHolder Type" forIndexPath:indexPath];
                    cell.StakeHolderTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.StakeHolderTF.text = ESHString;
                    cell.StakeHolderIV.hidden =YES;
                }
                else if (indexPath.row == 2) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Category" forIndexPath:indexPath];
                    cell.categoryTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.categoryTF.text = stakeholderCategoryString;
                    cell.categoryIV.hidden =YES;
                }
                else if (indexPath.row == 3) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Company" forIndexPath:indexPath];
                    cell.companyTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.companyTF.text = companyString;
                    cell.companyIV.hidden =YES;
                }
                else if (indexPath.row == 4) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Assigned To" forIndexPath:indexPath];
                    cell.assignedToTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.assignedToTF.text = assignedToString;
                    cell.assignedToIV.hidden =YES;
                }
                
                else if (indexPath.row == 5) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Target Date" forIndexPath:indexPath];
                    cell.targetDateTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.targetDateTF.text = targetDateCategoryString;
                    cell.targetDateIV.hidden =YES;
                }
                else if (indexPath.row == 6) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Action Type" forIndexPath:indexPath];
                    cell.actionTypeTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.actionTypeTF.text = actionTypeString;
                    cell.actionTypeIV.hidden =YES;
                }
                else if (indexPath.row == 7) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Action Description" forIndexPath:indexPath];
                    cell.actionDescriptionTV.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    [cell.actionDescriptionTV setText:textViewText];
                    cell.actionDescrptionIV.hidden =YES;
                }
                else if (indexPath.row == 8) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"submit" forIndexPath:indexPath];
                    if (_update) {
                        [cell.submitBtn setTitle:@"Update" forState:UIControlStateNormal];
                    }
                    else{
                        [cell.submitBtn setTitle:@"Submit" forState:UIControlStateNormal];
                    }
                }
            }
        }
        else{
            if(stakeHType==1)
            {
                if (indexPath.row == 0) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Category1" forIndexPath:indexPath];
                    cell.mainCategoryTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.mainCategoryTF.text = mainCategoryString;
                    //                    if(_update){
                    //                        cell.userInteractionEnabled = NO;
                    //                    }
                }
                else if (indexPath.row == 1) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"GroupCategory" forIndexPath:indexPath];
                    cell.groupCategory.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.groupCategory.text = groupCategoryString;
                    //                    if(_update){
                    //                        cell.userInteractionEnabled = NO;
                    //                    }
                }
                else if (indexPath.row == 2) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Employee Type" forIndexPath:indexPath];
                    cell.employeeIV.hidden =YES;
                    cell.employeeTypeTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.employeeTypeTF.text = actionForString;
                    
                }
                else if (indexPath.row == 3) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"StakeHolder Type" forIndexPath:indexPath];
                    cell.StakeHolderTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.StakeHolderTF.text = ESHString;
                    cell.StakeHolderIV.hidden =YES;
                }
                else if (indexPath.row == 4) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Assigned To" forIndexPath:indexPath];
                    cell.assignedToTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.assignedToTF.text = assignedToString;
                    cell.assignedToIV.hidden =YES;
                }
                else if (indexPath.row == 5) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Start Date" forIndexPath:indexPath];
                    cell.startDate.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.startDate.text = startDateString;
                    cell.startDateIV.hidden =YES;
                }
                else if (indexPath.row == 6) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Target Date" forIndexPath:indexPath];
                    cell.targetDateTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.targetDateTF.text = targetDateCategoryString;
                    cell.targetDateIV.hidden =YES;
                }
                else if (indexPath.row == 7) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Action Type" forIndexPath:indexPath];
                    cell.actionTypeTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.actionTypeTF.text = actionTypeString;
                    cell.actionTypeIV.hidden =YES;
                }
                else if (indexPath.row == 8) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Action Description" forIndexPath:indexPath];
                    cell.actionDescriptionTV.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    [cell.actionDescriptionTV setText:textViewText];
                    cell.actionDescrptionIV.hidden =YES;
                }
                else if (indexPath.row == 9) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"File Description" forIndexPath:indexPath];
                    cell.fileDescription.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    [cell.fileDescription setText:fileDescriptionTextViewText];
                }
                
                else if (indexPath.row == 10) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Document" forIndexPath:indexPath];
                }
                else if (indexPath.row > 10 && indexPath.row != 11+docArray.count) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"doc" forIndexPath:indexPath];
                    cell.docName.text = [[docArray objectAtIndex: indexPath.row-11 ] objectForKey:@"FileName"];
                    if ([[[docArray objectAtIndex: indexPath.row-11 ] objectForKey:@"ActionplannerID"] isEqualToString:@""]) {
                        cell.downloadBtn.hidden = YES;
                    }
                    else{
                        cell.downloadBtn.hidden = NO;
                    }
                }
                else  {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"submit" forIndexPath:indexPath];
                    if (_update) {
                        [cell.submitBtn setTitle:@"Update" forState:UIControlStateNormal];
                    }
                    else{
                        [cell.submitBtn setTitle:@"Submit" forState:UIControlStateNormal];
                    }
                }
            }
            else
            {
                if (indexPath.row == 0) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Category1" forIndexPath:indexPath];
                    cell.mainCategoryTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.mainCategoryTF.text = mainCategoryString;
                    //                    if(_update){
                    //                        cell.userInteractionEnabled = NO;
                    //                    }
                }
                else if (indexPath.row == 1) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"GroupCategory" forIndexPath:indexPath];
                    cell.groupCategory.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.groupCategory.text = groupCategoryString;
                    //                    if(_update){
                    //                        cell.userInteractionEnabled = NO;
                    //                    }
                }
                else if (indexPath.row == 2) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Employee Type" forIndexPath:indexPath];
                    cell.employeeIV.hidden =YES;
                    cell.employeeTypeTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.employeeTypeTF.text = actionForString;
                    
                }
                else if (indexPath.row == 3) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"StakeHolder Type" forIndexPath:indexPath];
                    cell.StakeHolderTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.StakeHolderTF.text = ESHString;
                    cell.StakeHolderIV.hidden =YES;
                }
                else if (indexPath.row == 4) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Category" forIndexPath:indexPath];
                    cell.categoryTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.categoryTF.text = stakeholderCategoryString;
                    cell.categoryIV.hidden =YES;
                }
                else if (indexPath.row == 5) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Company" forIndexPath:indexPath];
                    cell.companyTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.companyTF.text = companyString;
                    cell.companyIV.hidden =YES;
                }
                else if (indexPath.row == 6) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Assigned To" forIndexPath:indexPath];
                    cell.assignedToTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.assignedToTF.text = assignedToString;
                    cell.assignedToIV.hidden =YES;
                }
                else if (indexPath.row == 7) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Start Date" forIndexPath:indexPath];
                    cell.startDate.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.startDate.text = startDateString;
                    cell.startDateIV.hidden =YES;
                }
                else if (indexPath.row == 8) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Target Date" forIndexPath:indexPath];
                    cell.targetDateTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.targetDateTF.text = targetDateCategoryString;
                    cell.targetDateIV.hidden =YES;
                }
                else if (indexPath.row == 9) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Action Type" forIndexPath:indexPath];
                    cell.actionTypeTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.actionTypeTF.text = actionTypeString;
                    cell.actionTypeIV.hidden =YES;
                }
                else if (indexPath.row == 10) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Action Description" forIndexPath:indexPath];
                    cell.actionDescriptionTV.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    [cell.actionDescriptionTV setText:textViewText];
                    cell.actionDescrptionIV.hidden =YES;
                }
                else if (indexPath.row == 11) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"File Description" forIndexPath:indexPath];
                    cell.fileDescription.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    [cell.fileDescription setText:fileDescriptionTextViewText];
                }
                
                else if (indexPath.row == 12) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Document" forIndexPath:indexPath];
                }
                else if (indexPath.row > 12 && indexPath.row != 13+docArray.count) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"doc" forIndexPath:indexPath];
                    cell.docName.text = [[docArray objectAtIndex: indexPath.row-13 ] objectForKey:@"FileName"];
                    if ([[[docArray objectAtIndex: indexPath.row-13 ] objectForKey:@"ActionplannerID"] isEqualToString:@""]) {
                        cell.downloadBtn.hidden = YES;
                    }
                    else{
                        cell.downloadBtn.hidden = NO;
                    }
                }
                else  {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"submit" forIndexPath:indexPath];
                    if (_update) {
                        [cell.submitBtn setTitle:@"Update" forState:UIControlStateNormal];
                    }
                    else{
                        [cell.submitBtn setTitle:@"Submit" forState:UIControlStateNormal];
                    }
                }
            }
        }
    }
    
    
    return cell;
}
- (IBAction)actionTypeBtn:(id)sender {
    submitButtonClicked = NO;
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Select" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"Normal" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        actionTypeString = @"Normal";
        [dataTableView reloadData];
    }];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"Critical" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        actionTypeString = @"Critical";
        [dataTableView reloadData];
    }];
    
    [actionSheet addAction:action1];
    [actionSheet addAction:action2];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }]];
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}
- (IBAction)assignedBtn:(id)sender {
    if([Utitlity isConnectedTointernet]){
        if (stakeHType ==1) {
            sharedManager.passingMode = @"assignedToInternal";
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SearchViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
            [self presentViewController:myNavController animated:YES completion:nil];
            [dataTableView reloadData];
        }
        else  if ([companyString isEqualToString:@""]) {
            [self showMsgAlert:@"Please select Company"];
        }
        else{
            sharedManager.passingMode = @"assignedTo";
            sharedManager.passingId = Company;
            sharedManager.passingString = assignedToString;
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SearchViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
            [self presentViewController:myNavController animated:YES completion:nil];
            [dataTableView reloadData];
        }
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
- (IBAction)companyBtn:(id)sender {
    if([Utitlity isConnectedTointernet]){
        
        if ([stakeholderCategoryString isEqualToString:@""]) {
            [self showMsgAlert:@"Please select Catagory"];
        }
        else{
            sharedManager.passingMode = @"company";
            sharedManager.passingId = StakeholderCategory;
            sharedManager.passingString = companyString;
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SearchViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
            [self presentViewController:myNavController animated:YES completion:nil];
        }
        
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
    
}
- (IBAction)categoryBtn:(id)sender {
    if([Utitlity isConnectedTointernet]){
        if ([ESHString isEqualToString:@""]) {
            [self showMsgAlert:@"Please select the stacholder type"];
        }
        else{
            sharedManager.passingMode = @"category";
            sharedManager.passingString = stakeholderCategoryString;
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SearchViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
            [self presentViewController:myNavController animated:YES completion:nil];
        }
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
- (IBAction)employeeTypeBtn:(id)sender {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Select" message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"Self" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        employeeType = 0;
        actionForString = @"Self";
        [self clearTF];
    }];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"Other" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        employeeType = 1;
        actionForString = @"Other";
        [self clearTF];
    }];
    
    [actionSheet addAction:action1];
    [actionSheet addAction:action2];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }]];
    [self presentViewController:actionSheet animated:YES completion:nil];
}

-(void)clearTF{
    [dataTableView reloadData];
    if (!_update) {
        sharedManager.passingString = @"";
        textViewText =@"";
        fileDescriptionTextViewText =@"";
        stakeholderCategoryString= @"";
        companyString = @"";
        assignedToString= @"";
        startDateString = @"";
        targetDateCategoryString = @"";
        dateGap = NO;
        actionTypeString = @"";
        ESHString=@"";
        [fileAttachment removeAllObjects];
        if ([sharedManager.slideMenuSlected isEqualToString:@"yes"]) {
            mainCategoryString = @"";
            groupCategoryString = @"";
        }
    }
    else{
        stakeholderCategoryString= @"";
        companyString = @"";
        assignedToString= @"";
        ESHString=@"";
    }
    [dataTableView reloadData];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (employeeType == 0) {
        if (_meetingAction) {
            if (indexPath.row == 3) {
                return [self heightForText:textViewText withFont:[UIFont systemFontOfSize:14] andWidth:self.view.bounds.size.width-40]+70;
            }
        }
        else{
            if (indexPath.row == 6) {
                return [self heightForText:textViewText withFont:[UIFont systemFontOfSize:14] andWidth:self.view.bounds.size.width-40]+70;
            }
            if (indexPath.row == 7) {
                return [self heightForText:fileDescriptionTextViewText withFont:[UIFont systemFontOfSize:14] andWidth:self.view.bounds.size.width-40]+70;
            }
            
            else if (indexPath.row > 7 ) {
                return 44;
            }
        }
    }
    else{
        if (_meetingAction) {
            if (indexPath.row == 7) {
                return [self heightForText:textViewText withFont:[UIFont systemFontOfSize:14] andWidth:self.view.bounds.size.width-20]+70;
            }
        }
        else{
            if(stakeHType==2){
                if (indexPath.row == 10) {
                    return [self heightForText:textViewText withFont:[UIFont systemFontOfSize:14] andWidth:self.view.bounds.size.width-20]+70;
                }
                if (indexPath.row == 11) {
                    return [self heightForText:fileDescriptionTextViewText withFont:[UIFont systemFontOfSize:14] andWidth:self.view.bounds.size.width-20]+70;
                }
                else if (indexPath.row > 12 ) {
                    return 40;
                }
            }
            else{
                if (indexPath.row == 8) {
                    return [self heightForText:textViewText withFont:[UIFont systemFontOfSize:14] andWidth:self.view.bounds.size.width-20]+70;
                }
                if (indexPath.row == 9) {
                    return [self heightForText:fileDescriptionTextViewText withFont:[UIFont systemFontOfSize:14] andWidth:self.view.bounds.size.width-20]+70;
                }
                else if (indexPath.row > 10 ) {
                    return 40;
                }
            }
            
        }
    }
    return 80;
}
-(void)fillTheTF{
    if (![sharedManager.passingString isEqualToString:@""]) {
        clearData = NO;
        if ([sharedManager.passingMode isEqualToString: @"category"]) {
            companyString = @"";
            assignedToString = @"";
            stakeholderCategoryString = sharedManager.passingString;
            StakeholderCategory = sharedManager.passingId;
        }
        if ([sharedManager.passingMode isEqualToString: @"company"]) {
            assignedToString = @"";
            companyString= sharedManager.passingString;
            Company = sharedManager.passingId;
        }
        if ([sharedManager.passingMode isEqualToString: @"assignedTo"]) {
            assignedToString= sharedManager.passingString;
            AssignedTo = sharedManager.passingId;
        }
        if ([sharedManager.passingMode isEqualToString: @"assignedToInternal"]) {
            assignedToString= sharedManager.passingString;
            AssignedTo = sharedManager.passingId;
        }
        if ([sharedManager.passingMode isEqualToString: @"mainCategory"]) {
            mainCategoryString= sharedManager.passingString;
            mainCategoryID = sharedManager.passingId;
            groupCategoryString= @"";

            [self clearTF];
        }
        if ([sharedManager.passingMode isEqualToString: @"groupCategory"]) {
            groupCategoryString= sharedManager.passingString;
            groupCategoryID = sharedManager.passingId;
            
            [self clearTF];
        }
        [dataTableView reloadData];
    }
    
}
-(void)showMsgAlert:(NSString *)msg{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:nil
                                                                  message:msg
                                                           preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:ok];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)submitBtn:(id)sender {
    if([Utitlity isConnectedTointernet]){
        submitButtonClicked = YES;
        [self validateTF];
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
    
}
-(void)validateTF{
    NSString *msg=@"";
    NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    if ([[textViewText stringByTrimmingCharactersInSet: set] length] == 0){
        msg=[NSString stringWithFormat:@"Action Description is required"];
    }
    
    if ([actionTypeString isEqualToString:@""]) {
        msg=[NSString stringWithFormat:@"Action Type is required"];
    }
    
    
    if ([targetDateCategoryString isEqualToString:@""] ) {
        msg=[NSString stringWithFormat:@"Target Date is required"];
    }
    
    if ([startDateString isEqualToString:@""] && !_meetingAction) {
        msg=[NSString stringWithFormat:@"Start Date is required"];
    }
    if (!_meetingAction) {
        if ([groupCategoryString isEqualToString:@""]) {
            msg=[NSString stringWithFormat:@"Group category is required"];
        }
        
        if ([mainCategoryString isEqualToString:@""]) {
            msg=[NSString stringWithFormat:@"Group is required"];
        }
    }
    
    
    if (employeeType == 1) {
        if ([assignedToString isEqualToString:@""]) {
            msg=[NSString stringWithFormat:@"Assigned To is required"];
        }
        if (stakeHType == 2) {
            if ([companyString isEqualToString:@""]) {
                msg=[NSString stringWithFormat:@"Company is required"];
            }
            
            if ([stakeholderCategoryString isEqualToString:@""]) {
                msg=[NSString stringWithFormat:@"Category is required"];
                
            }
        }
        if ([ESHString isEqualToString:@""]) {
            msg=[NSString stringWithFormat:@"Stackholder type is required"];
        }
    }
    if (![msg isEqualToString: @""]) {
        [self showMsgAlert:msg];
    }
    else{
        if (_meetingAction) {
            [self submitMeetingApiCall];
        }
        else{
            [self submitApiCall];
        }
    }
}
-(void)submitMeetingApiCall{
    if([Utitlity isConnectedTointernet]){
        NSDictionary *dict;
        [formatter setDateFormat:@"yyyy-MM-dd"];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *ActionResponsible;
        if (employeeType == 0) {
            ActionResponsible = [defaults objectForKey:@"UserID"];
        }
        else{
            ActionResponsible = AssignedTo;
        }
        if (!_update) {
            NSLog(@"_meetingDate----%@",_meetingDate);
            dict= @{ @"MeetingDate" : _meetingDate,@"MeetingTypeID" : _meetingID,@"Mode" : @"INS", @"ActionID" : @0 ,@"ActionDescription" : textViewText,@"ActionResponsible" : ActionResponsible,@"TargetDate" : [formatter stringFromDate:curDate1], @"ModifiedBy" : [defaults objectForKey:@"UserID"] , @"ActionType" : actionTypeString   };
        }else{
            dict= @{ @"MeetingDate" : _meetingDate,@"MeetingTypeID" : _meetingID,@"Mode" : @"UPD", @"ActionID" : ActionplannerID ,@"ActionDescription" : textViewText,@"ActionResponsible" : ActionResponsible,@"TargetDate" : [formatter stringFromDate:curDate1], @"ModifiedBy" : [defaults objectForKey:@"UserID"] , @"ActionType" : actionTypeString   };
        }
        
        NSString* JsonString = [Utitlity JSONStringConv: dict];
        NSLog(@"dict-------%@",JsonString);
        
        [[WebServices sharedInstance]apiAuthwithJSON:PostAddAdhocMeetingActions HTTPmethod:@"POST" forparameters:JsonString ContentType:APICONTENTTYPE apiKey:nil onCompletion:^(NSDictionary *json, NSURLResponse * headerResponse) {
            
            NSString *error=[json valueForKey:@"error"];
            [self hideProgress];
            
            if(error.length>0){
                [self showMsgAlert:[json valueForKey:@"error_description"]];
                return ;
            }else{
                if(json.count==0){
                    [self showMsgAlert:@"Error , Try Again"];
                }else{
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Conduct Meeting" message:@"Action assigned successfully." preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                         {
                                             [self dismissViewControllerAnimated:YES completion:nil];
                                         }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];
                }
            }
            
        }];
        
    }
    else{
        [self showMsgAlert:NOInternetMessage];
    }
}
-(void)submitApiCall{
    clearData = YES;
    [self showProgress];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [params setObject:textViewText forKey:@"ActionDescription"];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    [params setObject:[formatter stringFromDate:curDate] forKey:@"ActionAssignDate"];
    [params setObject:[formatter stringFromDate:curDate1] forKey:@"TargetDate"];
    [params setObject:[NSString stringWithFormat:@"%d",employeeType] forKey:@"EmployeeType"];
    [params setObject:actionTypeString forKey:@"ActionType"];
    [params setObject:@"INS" forKey:@"ActionMode"];
    [params setObject:mainCategoryID forKey:@"GroupMasterID"];
    [params setObject:groupCategoryID forKey:@"CategoryID"];
    [params setObject:@"Assign" forKey:@"Status"];
    [params setObject:[defaults objectForKey:@"UserID"] forKey:@"CreatedBy"];
    if (fileDescriptionTextViewText) {
        [params setObject:fileDescriptionTextViewText forKey:@"FileDescription"];
    }
    
    if (_update) {
        [params setObject:ActionplannerID forKey:@"ActionplannerID"];
        for (int i =0; i< fileAttachment.count; i++) {
            [docArray removeObjectAtIndex:0];
        }
        NSMutableArray *arr = [[NSMutableArray alloc] init];
        for (int i = 0 ; i<docArray.count+docDeletedArray.count; i++) {
            NSDictionary*dits;
            if (i<docArray.count) {
                dits = @{@"DocumentAttachmentID":[[docArray objectAtIndex:i] objectForKey:@"DocumentAttachmentID"],@"FileDescription":[[docArray objectAtIndex:i] objectForKey:@"FileDescription"],@"IsDeleted":[[docArray objectAtIndex:i] objectForKey:@"IsDeleted"],@"ActionplannerID":[[docArray objectAtIndex:i] objectForKey:@"ActionplannerID"]};
            }
            else{
                dits = @{@"DocumentAttachmentID":[[docDeletedArray objectAtIndex:i-docArray.count] objectForKey:@"DocumentAttachmentID"],@"FileDescription":[[docDeletedArray objectAtIndex:i-docArray.count] objectForKey:@"FileDescription"],@"IsDeleted":[[docDeletedArray objectAtIndex:i-docArray.count] objectForKey:@"IsDeleted"],@"ActionplannerID":[[docDeletedArray objectAtIndex:i-docArray.count] objectForKey:@"ActionplannerID"]};
            }
            
            [arr addObject:dits];
        }
        NSError * errortest;
        NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:arr options:0 error:&errortest];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
        [params setObject:jsonString forKey:@"lstDoc"];
        
        if (employeeType == 0) {
            [params setObject:@"00000000-0000-0000-0000-000000000000" forKey:@"StakeholderCategory"];
            [params setObject:@"00000000-0000-0000-0000-000000000000" forKey:@"Company"];
            [params setObject:[defaults objectForKey:@"UserID"] forKey:@"AssignedTo"];
            [params setObject:[defaults objectForKey:@"UserID"] forKey:@"ActionOwner"];
            [params setObject:@1 forKey:@"ActionAssignedType"];
            [params setObject:[defaults objectForKey:@"UserID"] forKey:@"ActionAsignee"];
            [params setObject:@0 forKey:@"EmployeeType"];
        }
        else{
            [params setObject:AssignedTo forKey:@"AssignedTo"];
            [params setObject:AssignedTo forKey:@"ActionOwner"];
            [params setObject:@2 forKey:@"ActionAssignedType"];
            [params setObject:[defaults objectForKey:@"UserID"] forKey:@"ActionAsignee"];
            if (stakeHType == 2) {
                [params setObject:StakeholderCategory forKey:@"StakeholderCategory"];
                [params setObject:Company forKey:@"Company"];
                [params setObject:@1 forKey:@"EmployeeType"];
            }
            else{
                [params setObject:@"00000000-0000-0000-0000-000000000000" forKey:@"StakeholderCategory"];
                [params setObject:@"00000000-0000-0000-0000-000000000000" forKey:@"Company"];
                [params setObject:@1 forKey:@"EmployeeType"];
            }
            
        }
        NSString* JsonString = [Utitlity JSONStringConv: params];
        NSLog(@"JsonString-------%@",JsonString);
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
        [manager.requestSerializer setValue:@"multipart/form-data" forHTTPHeaderField:@"contentType"];
        AFHTTPRequestOperation *op =[manager POST:[NSString stringWithFormat:@"%@POSTUpdateActionPlannerbyID",BASEURL] parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
            for (int i = 0; i<fileAttachment.count; i++) {
                [formData appendPartWithFileData:[[fileAttachment objectAtIndex:i] objectForKey:@"fileData"] name:@"files" fileName:[[fileAttachment objectAtIndex:i] objectForKey:@"FileName"] mimeType:[[[fileAttachment objectAtIndex:i] objectForKey:@"FileName"] pathExtension]];
            }
        }
                                          success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
                                              [self hideProgress];
                                              UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Action updated successfully" preferredStyle:UIAlertControllerStyleAlert];
                                              UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                                                   {
                                                                       [self dismissViewControllerAnimated:YES completion:nil];
                                                                   }];
                                              UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
                                              [alert addAction:ok];
                                              [alert addAction:cancel];
                                              [self presentViewController:alert animated:YES completion:nil];
                                              
                                              NSLog(@"responseObject-------%@",responseObject);
                                          } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
                                              NSString *ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
                                              
                                              NSData *data1 = [ErrorResponse dataUsingEncoding:NSUTF8StringEncoding];
                                              id json = [NSJSONSerialization JSONObjectWithData:data1 options:0 error:nil];
                                              [self hideProgress];
                                              
                                              NSLog(@"%@",json);
                                          }];
        [op start];
    }
    else{
        
        if (employeeType == 0) {
            [params setObject:@"00000000-0000-0000-0000-000000000000" forKey:@"StakeholderCategory"];
            [params setObject:@"00000000-0000-0000-0000-000000000000" forKey:@"Company"];
            [params setObject:[defaults objectForKey:@"UserID"] forKey:@"AssignedTo"];
            [params setObject:[defaults objectForKey:@"UserID"] forKey:@"ActionOwner"];
            [params setObject:@1 forKey:@"ActionAssignedType"];
            [params setObject:[defaults objectForKey:@"UserID"] forKey:@"ActionAsignee"];
            [params setObject:@0 forKey:@"EmployeeType"];
        }
        else{
            
            [params setObject:AssignedTo forKey:@"AssignedTo"];
            [params setObject:AssignedTo forKey:@"ActionOwner"];
            [params setObject:@2 forKey:@"ActionAssignedType"];
            [params setObject:[defaults objectForKey:@"UserID"] forKey:@"ActionAsignee"];
            if (stakeHType == 2) {
                [params setObject:StakeholderCategory forKey:@"StakeholderCategory"];
                [params setObject:Company forKey:@"Company"];
                [params setObject:@2 forKey:@"EmployeeType"];
            }
            else{
                [params setObject:@"00000000-0000-0000-0000-000000000000" forKey:@"StakeholderCategory"];
                [params setObject:@"00000000-0000-0000-0000-000000000000" forKey:@"Company"];
                [params setObject:@1 forKey:@"EmployeeType"];
            }
            
            
        }
        [self showProgress];
        //        NSString* JsonString = [Utitlity JSONStringConv: params];
        NSLog(@"JsonString-------%@",params);
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
        [manager.requestSerializer setValue:@"multipart/form-data" forHTTPHeaderField:@"contentType"];
        AFHTTPRequestOperation *op =[manager POST:
                                     [NSString stringWithFormat:@"%@POSTInsertAPWithDocuments",BASEURL] parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
                                         for (int i = 0; i<fileAttachment.count; i++) {
                                             [formData appendPartWithFileData:[[fileAttachment objectAtIndex:i] objectForKey:@"fileData"] name:@"files" fileName:[[fileAttachment objectAtIndex:i] objectForKey:@"FileName"] mimeType:[[[fileAttachment objectAtIndex:i] objectForKey:@"FileName"] pathExtension]];
                                         }
                                         
                                     }
                                          success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
                                              [self hideProgress];
                                              NSLog(@"responseObject-------%@",responseObject);
                                              
                                              UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Action added successfully" preferredStyle:UIAlertControllerStyleAlert];
                                              
                                              UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                                                   {
                                                                       [self dismissViewControllerAnimated:YES completion:nil];
                                                                   }];
                                              UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
                                              [alert addAction:ok];
                                              [alert addAction:cancel];
                                              [self presentViewController:alert animated:YES completion:nil];
                                              
                                          } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
                                              NSString *ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
                                              
                                              NSData *data1 = [ErrorResponse dataUsingEncoding:NSUTF8StringEncoding];
                                              id json = [NSJSONSerialization JSONObjectWithData:data1 options:0 error:nil];
                                              [self hideProgress];
                                              
                                              NSLog(@"%@",error);
                                          }];
        [op start];
    }
    
    
}
-(void)showProgress{
    [self hideProgress];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
}
-(void)hideProgress{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    NSString *newString = [textView.text stringByReplacingCharactersInRange:range withString:text];
    NSIndexPath *ip ;
    NSIndexPath *ip1 ;
    
    if (employeeType == 0) {
        if (_meetingAction) {
            ip = [NSIndexPath indexPathForRow:3 inSection:0];
        }
        else{
            ip = [NSIndexPath indexPathForRow:6 inSection:0];
            ip1 = [NSIndexPath indexPathForRow:7 inSection:0];
        }
    }
    else{
        if (_meetingAction) {
            if (stakeHType ==2) {
                ip = [NSIndexPath indexPathForRow:7 inSection:0];
            }
            else{
                ip = [NSIndexPath indexPathForRow:5 inSection:0];
            }
        }
        else{
            if (stakeHType ==2) {
                ip = [NSIndexPath indexPathForRow:10 inSection:0];
                ip1 = [NSIndexPath indexPathForRow:11 inSection:0];
            }
            else{
                ip = [NSIndexPath indexPathForRow:8 inSection:0];
                ip1 = [NSIndexPath indexPathForRow:9 inSection:0];
            }
            
        }
    }
    CustomTableViewCell *cell = [dataTableView cellForRowAtIndexPath:ip];
    CustomTableViewCell *cell1 = [dataTableView cellForRowAtIndexPath:ip1];
    if (textView ==cell.actionDescriptionTV) {
        textViewText = newString;
    }
    else if (textView ==cell1.fileDescription) {
        fileDescriptionTextViewText = newString;
    }
    
    return YES;
}


-(CGFloat)heightForText:(NSString*)text withFont:(UIFont *)font andWidth:(CGFloat)width
{
    CGSize constrainedSize = CGSizeMake(width, MAXFLOAT);
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName,nil];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:text attributes:attributesDictionary];
    CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    if (requiredHeight.size.width > width) {
        requiredHeight = CGRectMake(0,0,width, requiredHeight.size.height);
    }
    return requiredHeight.size.height;
}

- (void)textViewDidChange:(UITextView *)textView
{
    CGFloat fixedWidth = textView.frame.size.width;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = textView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    [dataTableView beginUpdates];
    textView.frame = newFrame;
    [dataTableView endUpdates];
    //    if ([textView.text rangeOfString:@"\n\n"].location == NSNotFound) return;
    //    NSString *resultStr = [textView.text stringByReplacingOccurrencesOfString:@"\n\n" withString:@"\n"];
    //    textView.text = resultStr;
}
- (IBAction)backBtn:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)loadDataFromApi{
    if([Utitlity isConnectedTointernet]){
        [self showProgress];
        
        NSString *targetUrl = [NSString stringWithFormat:@"%@GetActionPlannerByID?ActPlannerID=%@", BASEURL,sharedManager.passingId];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setHTTPMethod:@"GET"];
        [request setURL:[NSURL URLWithString:targetUrl]];
        
        [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:
          ^(NSData * _Nullable data,
            NSURLResponse * _Nullable response,
            NSError * _Nullable error) {
              NSArray * actionDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
              NSLog(@"actionDict----------%@",actionDict);
              
              dispatch_async(dispatch_get_main_queue(), ^{
                  [self hideProgress];
                  [formatter setDateFormat:@"dd/MM/yyy"];
                  if ([[[actionDict objectAtIndex:0] objectForKey:@"ActionDescription"] length] > 1) {
                      textViewText = [[[actionDict objectAtIndex:0] objectForKey:@"ActionDescription"] substringToIndex:[[[actionDict objectAtIndex:0] objectForKey:@"ActionDescription"] length] ];
                  } else {
                      textViewText  = [[actionDict objectAtIndex:0] objectForKey:@"ActionDescription"];
                  }
                  mainCategoryString = [[actionDict objectAtIndex:0] objectForKey:@"GroupDescription"];
                  mainCategoryID = [[actionDict objectAtIndex:0] objectForKey:@"GroupMasterID"];
                  groupCategoryString = [[actionDict objectAtIndex:0] objectForKey:@"CategoryDescription"];
                  groupCategoryID = [[actionDict objectAtIndex:0] objectForKey:@"CategoryID"];
                  actionTypeString = [[actionDict objectAtIndex:0] objectForKey:@"ActionType"];
                  assignedToString = [[actionDict objectAtIndex:0] objectForKey:@"Owner"];
                  AssignedTo = [[actionDict objectAtIndex:0] objectForKey:@"ActionOwner"];
                  if ([[actionDict objectAtIndex:0] objectForKey:@"CompanyName"]!= [NSNull null]) {
                      companyString = [[actionDict objectAtIndex:0] objectForKey:@"CompanyName"];
                  }
                  else{
                      companyString = @"";
                  }
                  Company = [[actionDict objectAtIndex:0] objectForKey:@"Company"];
                  StakeholderCategory = [[actionDict objectAtIndex:0] objectForKey:@"StakeholderCategory"];
                  if ([[actionDict objectAtIndex:0] objectForKey:@"CategoryName"] != [NSNull null]) {
                      stakeholderCategoryString = [[actionDict objectAtIndex:0] objectForKey:@"CategoryName"];
                  }
                  else{
                      stakeholderCategoryString = @"";
                  }
                  startDateString = [[actionDict objectAtIndex:0] objectForKey:@"ActionAssignDate"];
                  curDate = [formatter dateFromString:startDateString];
                  targetDateCategoryString = [[actionDict objectAtIndex:0] objectForKey:@"TargetDate"];
                  curDate1 = [formatter dateFromString:targetDateCategoryString];
                  ActionplannerID = [[actionDict objectAtIndex:0] objectForKey:@"ActionplannerID"];
                  fileDescriptionTextViewText = @"";
                  docArray = [[[actionDict objectAtIndex:0] objectForKey:@"lstDoc"] mutableCopy];
                  if ([[[actionDict objectAtIndex:0] objectForKey:@"EmployeeType"] intValue] == 0) {
                      employeeType = 0;
                      actionForString = @"Self";
                  }
                  else{
                      employeeType = 1;
                      actionForString = @"Other";
                      if ([[[actionDict objectAtIndex:0] objectForKey:@"Company"] isEqualToString:@"00000000-0000-0000-0000-000000000000"]) {
                          stakeHType = 1;
                          ESHString = @"Internal StakeHolders";
                      }
                      else{
                          stakeHType = 2;
                          ESHString = @"External StakeHolders";
                      }
                  }
                  [dataTableView reloadData];
              });
              
          }] resume];
        
        
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}

-(void)loadDataFromMeetingDict{
    
    [formatter setDateFormat:@"dd/MM/yyy"];
    textViewText  = [_meetingActionDict objectForKey:@"ActionDescription"];
    actionTypeString = [_meetingActionDict objectForKey:@"ActionType"];
    assignedToString = [_meetingActionDict objectForKey:@"EmpName"];
    AssignedTo = [_meetingActionDict objectForKey:@"ActionResponsible"];
    if ([_meetingActionDict objectForKey:@"Name"]!= [NSNull null]) {
        companyString = [_meetingActionDict objectForKey:@"Name"];
    }
    else{
        companyString = @"";
    }
    Company = [_meetingActionDict objectForKey:@"ExternalStakeholderID"];
    StakeholderCategory = [_meetingActionDict objectForKey:@"ExternalStakeholderTypeID"];
    if ([_meetingActionDict objectForKey:@"Type"] != [NSNull null]) {
        stakeholderCategoryString = [_meetingActionDict objectForKey:@"Type"];
    }
    else{
        stakeholderCategoryString = @"";
    }
    targetDateCategoryString = [_meetingActionDict objectForKey:@"TargetDate"];
    curDate1 = [formatter dateFromString:targetDateCategoryString];
    if ([[_meetingActionDict objectForKey:@"ActionResponsible"] isEqualToString:[_meetingActionDict objectForKey:@"CreatedBy"]]) {
        employeeType = 0;
    }
    else{
        employeeType = 1;
        
    }
    ActionplannerID = [_meetingActionDict objectForKey:@"ActionID"];
    
    if (employeeType == 0) {
        actionForString = @"Self";
    }
    else{
        actionForString = @"Other";
        if ([[_meetingActionDict objectForKey:@"ExternalStakeholderID"] isEqualToString:@"00000000-0000-0000-0000-000000000000"]) {
            stakeHType = 1;
            ESHString = @"Internal StakeHolders";
        }
        else{
            stakeHType = 2;
            ESHString = @"External StakeHolders";
        }
    }
    [dataTableView reloadData];
    
}
- (IBAction)UploadDoc:(UIButton *)sender {
    
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Select" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"Take a photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        idPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:idPicker animated:YES completion:nil];
    }];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"Choose from Libary" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSArray* types = @[(NSString*)kUTTypeImage,(NSString*)kUTTypeSpreadsheet,(NSString*)kUTTypePresentation,(NSString*)kUTTypeDatabase,(NSString*)kUTTypeFolder,(NSString*)kUTTypeZipArchive,(NSString*)kUTTypeVideo,(NSString*)kUTTypePDF,(__bridge NSString* ) kUTTypeContent,(__bridge NSString *) kUTTypeData,(__bridge NSString* ) kUTTypePackage,(__bridge NSString *) kUTTypeDiskImage,(NSString* )kUTTypeCompositeContent,@"com.apple.iwork.pages.pages",@"com.apple.iwork.numbers.numbers",@"com.apple.iwork.keynote.key"];
        UIDocumentPickerViewController *docPicker = [[UIDocumentPickerViewController alloc] initWithDocumentTypes:types inMode:UIDocumentPickerModeImport];
        docPicker.delegate = self;
        [self presentViewController:docPicker animated:YES completion:nil];
        
    }];
    
    
    [actionSheet addAction:action1];
    [actionSheet addAction:action2];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }]];
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}
- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentAtURL:(NSURL *)url
{
    [self hideProgress];
    if (controller.documentPickerMode == UIDocumentPickerModeImport)
    {
        // Condition called when user download the file
        fileData = [NSData dataWithContentsOfURL:url];
        // NSData of the content that was downloaded - Use this to upload on the server or save locally in directory
        //Showing alert for success
        filepath=url;
        alertMessage = [NSString stringWithFormat:@"%@", [url lastPathComponent]];
        NSDictionary *dic = @{@"FileName":alertMessage,@"ActionplannerID":@"",@"DocumentAttachmentID":@"",@"FileDescription":@"",@"IsDeleted":@""};
        [docArray insertObject:dic atIndex:0];
        NSDictionary *dic1 = @{@"FileName":alertMessage,@"fileData":fileData,@"filepath":filepath};
        [fileAttachment addObject:dic1];
        [dataTableView reloadData];
    }
    else  if (controller.documentPickerMode == UIDocumentPickerModeExportToService)
    {
        // Called when user uploaded the file - Display success alert
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"nsurl===%@",url);
            alertMessage = [NSString stringWithFormat:@"Successfully uploaded file %@", [url lastPathComponent]];
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:@"UIDocumentView"
                                                  message:alertMessage
                                                  preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alertController animated:YES completion:nil];
            alertMessage = nil;
            filepath = nil;
        });
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
        
        UIImage* cameraImage = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        fileData = UIImageJPEGRepresentation(cameraImage, 0);
        
        ALAssetsLibrary* assetsLibrary = [[ALAssetsLibrary alloc] init];
        
        [assetsLibrary writeImageToSavedPhotosAlbum:cameraImage.CGImage
                                           metadata:[info objectForKey:UIImagePickerControllerMediaMetadata]
                                    completionBlock:^(NSURL *assetURL, NSError *error) {
                                        
                                        if (!error) {
                                            filepath=assetURL;
                                            alertMessage = [NSString stringWithFormat:@"%@", [filepath lastPathComponent]];
                                            NSDictionary *dic = @{@"FileName":alertMessage,@"ActionplannerID":@"",@"DocumentAttachmentID":@"",@"FileDescription":@"",@"IsDeleted":@""};
                                            NSDictionary *dic1 = @{@"FileName":alertMessage,@"fileData":fileData,@"filepath":filepath};
                                            [fileAttachment addObject:dic1];
                                            [docArray insertObject:dic atIndex:0];
                                            [dataTableView reloadData];
                                        }
                                    }];
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}
- (IBAction)DeleteDoc:(UIButton *)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Are you sure do you want to delete the file" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                         {
                             NSIndexPath *indexPath = [dataTableView indexPathForCell:(UITableViewCell *)sender.superview.superview];
                             NSMutableDictionary *deleteDict = [[NSMutableDictionary alloc] init];
                             if (employeeType == 0) {
                                 deleteDict = [docArray[indexPath.row-9] mutableCopy];
                                 [deleteDict setObject:@"YES" forKey:@"IsDeleted"];
                                 if ([[deleteDict objectForKey:@"ActionplannerID"] isEqualToString:@""]) {
                                     alertMessage = nil;
                                     filepath = nil;
                                 }
                                 [docDeletedArray addObject:deleteDict];
                                 [docArray removeObjectAtIndex:indexPath.row-9];
                                 
                             }
                             else{
                                 NSLog(@"[indexPath.row-11]-----%ld",indexPath.row);
                                 if (stakeHType == 1) {
                                     deleteDict = [docArray[indexPath.row-11] mutableCopy];
                                 }
                                 else{
                                     deleteDict = [docArray[indexPath.row-13] mutableCopy];
                                 }
                                 [deleteDict setObject:@"YES" forKey:@"IsDeleted"];
                                 if ([[deleteDict objectForKey:@"ActionplannerID"] isEqualToString:@""]) {
                                     alertMessage = @"";
                                 }
                                 [docDeletedArray addObject:deleteDict];
                                 if (stakeHType == 1) {
                                     [docArray removeObjectAtIndex:indexPath.row-11];
                                 }
                                 else{
                                     [docArray removeObjectAtIndex:indexPath.row-13];
                                 }
                                 
                             }
                             [dataTableView reloadData];
                         }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:ok];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
    
}
- (IBAction)DownloadDoc:(UIButton *)sender {
    //download the file in a seperate thread.
    NSIndexPath *indexPath = [dataTableView indexPathForCell:(UITableViewCell *)sender.superview.superview];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Do you want to dowload or view the file" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *Download = [UIAlertAction actionWithTitle:@"Download" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                               {
                                   [self downloadFiles:indexPath];
                               }];
    UIAlertAction *viewFile = [UIAlertAction actionWithTitle:@"View" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                               {
                                   [self viewFile:indexPath];
                               }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:Download];
    [alert addAction:viewFile];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
    
    
}
-(void)viewFile:(NSIndexPath*)indexPath{
    if([Utitlity isConnectedTointernet]){
        popOverView.hidden = NO;
        
        [MBProgressHUD showHUDAddedTo:webView animated:YES];
        NSString *pdfUrl ;
        if (employeeType == 0) {
            pdfUrl = [NSString stringWithFormat:@"%@",[[docArray objectAtIndex:indexPath.row-9] objectForKey:@"VirtualPath"]];
        }
        else{
            if (stakeHType == 1) {
                pdfUrl = [NSString stringWithFormat:@"%@",[[docArray objectAtIndex:indexPath.row-11] objectForKey:@"VirtualPath"]];
            }
            else{
                pdfUrl = [NSString stringWithFormat:@"%@",[[docArray objectAtIndex:indexPath.row-13] objectForKey:@"VirtualPath"]];
            }
        }
        pdfUrl = [pdfUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:pdfUrl]];
        [webView loadRequest:request];
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
- (void)webViewDidFinishLoad:(UIWebView *)webView;
{
    [MBProgressHUD hideHUDForView:webView animated:YES];
}
-(void)downloadFiles :(NSIndexPath*)indexPath{
    [self showProgress];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *urlToDownload ;
        NSString *type;
        if (employeeType == 0) {
            urlToDownload = [NSString stringWithFormat:@"%@ActionFile?apid=%@&fileName=%@",BASEURL,[[docArray objectAtIndex:indexPath.row-9] objectForKey:@"ActionplannerID"],[[docArray objectAtIndex:indexPath.row-9] objectForKey:@"FileName"]];
            type=[[docArray objectAtIndex:indexPath.row-9] objectForKey:@"FileName"];
        }
        else{
            if (stakeHType == 1) {
                
                urlToDownload = [NSString stringWithFormat:@"%@ActionFile?apid=%@&fileName=%@",BASEURL,[[docArray objectAtIndex:indexPath.row-11] objectForKey:@"ActionplannerID"],[[docArray objectAtIndex:indexPath.row-11] objectForKey:@"FileName"]];
                type=[[docArray objectAtIndex:indexPath.row-11] objectForKey:@"FileName"];
                
            }
            else{
                urlToDownload = [NSString stringWithFormat:@"%@ActionFile?apid=%@&fileName=%@",BASEURL,[[docArray objectAtIndex:indexPath.row-13] objectForKey:@"ActionplannerID"],[[docArray objectAtIndex:indexPath.row-13] objectForKey:@"FileName"]];
                type=[[docArray objectAtIndex:indexPath.row-13] objectForKey:@"FileName"];
                
            }
        }
        NSLog(@"urlToDownload--------%@",urlToDownload);
        urlToDownload = [urlToDownload stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        //        urlToDownload = [urlToDownload stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        
        NSURL  *url = [NSURL URLWithString:urlToDownload];
        NSData *urlData = [NSData dataWithContentsOfURL:url];
        if ( urlData )
        {
            NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString  *documentsDirectory = [paths objectAtIndex:0];
            NSString  *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,type];
            
            //saving is done on main thread
            dispatch_async(dispatch_get_main_queue(), ^{
                [self hideProgress];
                [urlData writeToFile:filePath atomically:YES];
                NSLog(@"filepath test n---------%@",filePath);
                
                NSLog(@"File Saved !");
                
                //Create the file path of the document to upload
                NSURL *filePathToUpload = [NSURL fileURLWithPath:filePath]  ;
                UIDocumentPickerViewController *docPicker = [[UIDocumentPickerViewController alloc] initWithURL:filePathToUpload inMode:UIDocumentPickerModeExportToService];
                NSLog(@"filepath---------%@",filePathToUpload);
                docPicker.delegate = self;
                [self presentViewController:docPicker animated:YES completion:nil];
            });
        }
    });
}

- (IBAction)StakeHType:(UIButton *)sender {
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Select" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"External StakeHolders" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        stakeHType = 2;
        ESHString = @"External StakeHolders";
        [dataTableView reloadData];
        stakeholderCategoryString = @"";
        companyString = @"";
        assignedToString = @"";
        //        [self clearTF];
    }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"Internal StakeHolders" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        stakeHType = 1;
        ESHString =@"Internal StakeHolders";
        stakeholderCategoryString = @"";
        companyString = @"";
        assignedToString = @"";
        [dataTableView reloadData];
        //        [self clearTF];
    }];
    [actionSheet addAction:action1];
    [actionSheet addAction:action2];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }]];
    [self presentViewController:actionSheet animated:YES completion:nil];
}
- (IBAction)mainCategoryBtn:(id)sender {
    if([Utitlity isConnectedTointernet]){
        sharedManager.passingMode = @"mainCategory";
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        SearchViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
        [self presentViewController:myNavController animated:YES completion:nil];
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
- (IBAction)groupCategoryBtn:(id)sender {
    if([Utitlity isConnectedTointernet]){
        if ([mainCategoryString isEqualToString:@""]) {
            [self showMsgAlert:@"Please select Group"];
        }
        else{
            sharedManager.passingMode = @"groupCategory";
            sharedManager.passingId = mainCategoryID;
            //            sharedManager.passingString = companyString;
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SearchViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
            [self presentViewController:myNavController animated:YES completion:nil];
        }
        
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
- (IBAction)popOverCloseBtn:(id)sender {
    popOverView.hidden = YES;
}

@end

