//
//  InvoiceDashboardVC.m
//  test
//
//  Created by ceaselez on 30/07/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "InvoiceDashboardVC.h"
#import "test-Swift.h"
#import "IntAxisValueFormatter.h"
#import "AFHTTPRequestOperationManager.h"
#import "AFHTTPRequestOperation.h"
#import "AFHTTPSessionManager.h"
#import "GlobalURL.h"
#import "SearchViewController.h"
#import "Utitlity.h"
#import "Reachability.h"
#import "WebServices.h"
#import "MySharedManager.h"
#import "MBProgressHUD.h"
#import "SWRevealViewController.h"


@interface InvoiceDashboardVC ()<ChartViewDelegate,IChartAxisValueFormatter>
@property (nonatomic, strong) IBOutlet BarChartView *chartView;
@property (weak, nonatomic) IBOutlet UIButton *fiscalYearBtn;
@property (weak, nonatomic) IBOutlet UIButton *menuBtn;

@end

@implementation InvoiceDashboardVC
{
    NSMutableArray *dataArray;
    int xaxiscount;
    int yaxisVal;
    NSMutableArray *xVals;
    NSMutableArray *markernamesarray;
    MySharedManager *sharedManager;
    NSString *fiscalId;
//    BalloonMarker *marker;
}

- (void)viewDidLoad
{
    SWRevealViewController *revealViewController = self.revealViewController;
    
    if ( revealViewController )
    {
        [_menuBtn addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    [super viewDidLoad];
    dataArray=[NSMutableArray new];
    xVals=[NSMutableArray new];
    markernamesarray=[NSMutableArray new];
    sharedManager = [MySharedManager sharedManager];

    _chartView.delegate = self;
    _chartView.chartDescription.enabled = NO;
    _chartView.pinchZoomEnabled = NO;
    _chartView.drawBarShadowEnabled = NO;
    _chartView.drawGridBackgroundEnabled = NO;
    _chartView.doubleTapToZoomEnabled=NO;
    [_chartView zoomIn];
    [_chartView zoomWithScaleX:3 scaleY:0 x:0 y:0];
//    marker = [[BalloonMarker alloc]
//              initWithColor: [UIColor colorWithWhite:180/255. alpha:1.0]
//              font: [UIFont systemFontOfSize:12.0]
//              textColor: UIColor.whiteColor
//              insets: UIEdgeInsetsMake(8.0, 8.0, 20.0, 8.0)];
//    marker.chartView = _chartView;
//
//    marker.minimumSize = CGSizeMake(80.f, 40.f);
//    _chartView.marker = marker;
    
    ChartLegend *legend = _chartView.legend;
    legend.horizontalAlignment = ChartLegendHorizontalAlignmentLeft;
    legend.verticalAlignment = ChartLegendVerticalAlignmentBottom;
    legend.orientation = ChartLegendOrientationHorizontal;
    legend.drawInside = NO;
    legend.font = [UIFont fontWithName:@"Avenir-Medium" size:12.f];
    legend.yOffset = 10.0;
    legend.xOffset = 10.0;
    legend.yEntrySpace = 0.0;
    
    ChartXAxis *xAxis = _chartView.xAxis;
    xAxis.labelFont = [UIFont fontWithName:@"Avenir-Medium" size:10.f];
    xAxis.labelPosition = XAxisLabelPositionBottom;
    xAxis.labelRotationAngle=0;
    xAxis.granularity = 1.f;
    xAxis.centerAxisLabelsEnabled = YES;
    xAxis.valueFormatter = self;
    
    NSNumberFormatter *leftAxisFormatter = [[NSNumberFormatter alloc] init];
    leftAxisFormatter.maximumFractionDigits = 1;
    
    ChartYAxis *leftAxis = _chartView.leftAxis;
    leftAxis.labelFont = [UIFont fontWithName:@"Avenir-Medium" size:10.f];
    leftAxis.drawGridLinesEnabled = NO;
    leftAxis.spaceTop = 0.35;
    leftAxis.axisMinimum = 0;
    
    _chartView.rightAxis.enabled = NO;
}
-(void) viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    if ([sharedManager.slideMenuSlected isEqualToString:@"yes"]) {
        fiscalId = @"NA";
        [_fiscalYearBtn setTitle:@"----Select----" forState:UIControlStateNormal];
    }
    else{
        [self fillData];
    }
    [self GraphApi];
}
-(void)viewDidAppear:(BOOL)animated{
    if ([sharedManager.slideMenuSlected isEqualToString:@"yes"]) {
        sharedManager.slideMenuSlected = @"no";
    }
}

- (NSString * _Nonnull)stringForValue:(double)value axis:(ChartAxisBase * _Nullable)axis
{
    NSString *xAxisStringValue = @"";
    int myInt = (int)value;
    if(xVals.count > myInt)
    {
        xAxisStringValue = [xVals objectAtIndex:myInt];
        return xAxisStringValue;
    }
    else
    {
        return 0;
    }
}
- (NSString * _Nonnull)stringForDisplayValue:(double)value axis:(ChartAxisBase * _Nullable)axis
{
    NSString *xAxisStringValue = @"";
    int myInt = (int)value;
    if(markernamesarray.count > myInt)
    {
        xAxisStringValue = [markernamesarray objectAtIndex:myInt];
        return xAxisStringValue;
    }
    else
    {
        return 0;
    }
}
-(void)GraphApi
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSString *URL=[NSString stringWithFormat:@"%@GetInvoiceReceiptSummary?fiscalyearid=%@",BASEURL,fiscalId ];
    
    [manager GET:URL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         dispatch_async(dispatch_get_main_queue(), ^
                        {
                            //                                [[AppDelegate sharedDelegate]stopHUD];
                        });
         NSError *error = nil;
         dataArray = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];

         for (int i=0;i<[dataArray count];i++)
         {
             [xVals addObject:[[dataArray valueForKey:@"Description"]objectAtIndex:i]];
             [markernamesarray addObject:[[dataArray valueForKey:@"Description"]objectAtIndex:i]];
         }
         NSLog(@"dataArray-------%@",dataArray);
         [self updateChartData];
     }
         failure:
     ^(AFHTTPRequestOperation* operation, NSError* error) {
         dispatch_async(dispatch_get_main_queue(), ^{
             //                 [[AppDelegate sharedDelegate]stopHUD];
         });
         dispatch_async(dispatch_get_main_queue(), ^{
         });
     }];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateChartData
{
    if (self.shouldHideData)
    {
        _chartView.data = nil;
        return;
    }
    xaxiscount=(int)[dataArray count];
    [self setDataCount:xaxiscount range:yaxisVal];
}

- (void)setDataCount:(int)count range:(double)range
{
    float groupSpace = 0.1f;
    float barSpace = 0.0f;
    float barWidth = 0.3f;
    // (0.2 + 0.03) * 4 + 0.08 = 1.00 -> interval per "group"
    
    NSMutableArray *yVals1 = [[NSMutableArray alloc] init];
    NSMutableArray *yVals2 = [[NSMutableArray alloc] init];
    NSMutableArray *yVals3 = [[NSMutableArray alloc] init];
    
    int groupCount = count;
    int startYear = 0;
    int endYear = startYear + groupCount;
    
    for (int i = startYear; i < endYear; i++)
    {
        [yVals1 addObject:[[BarChartDataEntry alloc]
                           initWithX:i
                           y:[[[dataArray valueForKey:@"RaisedAmount"]objectAtIndex:i] doubleValue]]];
        
        [yVals2 addObject:[[BarChartDataEntry alloc]
                           initWithX:i
                           y:[[[dataArray valueForKey:@"ReceivedAmount"]objectAtIndex:i] doubleValue]]];
        
        [yVals3 addObject:[[BarChartDataEntry alloc]
                                   initWithX:i
                                   y:[[[dataArray valueForKey:@"ScheduledAmount"]objectAtIndex:i] doubleValue]]];
    }
    
    BarChartDataSet *set1 = nil, *set2 = nil, *set3 = nil;
    if (_chartView.data.dataSetCount > 0)
    {
        set1 = (BarChartDataSet *)_chartView.data.dataSets[0];
        set2 = (BarChartDataSet *)_chartView.data.dataSets[1];
        set3 = (BarChartDataSet *)_chartView.data.dataSets[2];
        
        set1.values = yVals1;
        set2.values = yVals2;
        set3.values = yVals3;
        
        BarChartData *data = _chartView.barData;
        
        _chartView.xAxis.axisMinimum = startYear;
        _chartView.xAxis.axisMaximum = [data groupWidthWithGroupSpace:groupSpace barSpace: barSpace] * xaxiscount + startYear;
        [data groupBarsFromX: startYear groupSpace: groupSpace barSpace: barSpace];
        [_chartView.data notifyDataChanged];
        [_chartView notifyDataSetChanged];
    }
    else
    {
        set1 = [[BarChartDataSet alloc] initWithValues:yVals1 label:@"Raised Amount"];
        [set1 setColor:[UIColor colorWithRed:104/255.f green:241/255.f blue:175/255.f alpha:1.f]];
        
        set2 = [[BarChartDataSet alloc] initWithValues:yVals2 label:@"Received Amount"];
        [set2 setColor:[UIColor colorWithRed:164/255.f green:228/255.f blue:251/255.f alpha:1.f]];
        
        set3 = [[BarChartDataSet alloc] initWithValues:yVals3 label:@"Scheduled Amount"];
        [set3 setColor:[UIColor colorWithRed:242/255.f green:247/255.f blue:158/255.f alpha:1.f]];
        
        NSMutableArray *dataSets = [[NSMutableArray alloc] init];
        [dataSets addObject:set1];
        [dataSets addObject:set2];
        [dataSets addObject:set3];
        
        BarChartData *data = [[BarChartData alloc] initWithDataSets:dataSets];
        [data setValueFont:[UIFont fontWithName:@"Avenir-Medium" size:10.f]];
        
        
        // specify the width each bar should have
        data.barWidth = barWidth;
        
        // restrict the x-axis range
        _chartView.xAxis.axisMinimum = startYear;
        
        // groupWidthWithGroupSpace(...) is a helper that calculates the width each group needs based on the provided parameters
        _chartView.xAxis.axisMaximum = startYear + [data groupWidthWithGroupSpace:groupSpace barSpace: barSpace] * groupCount;
        
        [data groupBarsFromX: startYear groupSpace: groupSpace barSpace: barSpace];
        
        _chartView.data = data;
        [_chartView.data notifyDataChanged];
        [_chartView notifyDataSetChanged];
    }
}

- (void)optionTapped:(NSString *)key
{
    [super handleOption:key forChartView:_chartView];
}


#pragma mark - ChartViewDelegate

//- (void)chartValueSelected:(ChartViewBase * __nonnull)chartView entry:(ChartDataEntry * __nonnull)entry highlight:(ChartHighlight * __nonnull)highlight
//{
//    [marker setLabel:[chartView.xAxis.valueFormatter stringForValue:entry.x axis:chartView.xAxis]];
//}

-(void)showProgress{
    [MBProgressHUD showHUDAddedTo:_chartView animated:YES];
    
}

-(void)hideProgress{
    [MBProgressHUD hideHUDForView:_chartView animated:YES];
}
-(void)showMsgAlert:(NSString *)msg{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:nil
                                                                  message:msg
                                                           preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:ok];
    
    
    [self presentViewController:alert animated:YES completion:nil];
}


- (IBAction)ppysicalYearBtn:(id)sender {
    if([Utitlity isConnectedTointernet]){
        sharedManager.passingMode = @"physicalYear";
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        SearchViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
        [self presentViewController:myNavController animated:YES completion:nil];
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
-(void)fillData{
    if (sharedManager.passingString.length != 0) {
        
        if ([sharedManager.passingMode isEqualToString: @"physicalYear"]) {
            [_fiscalYearBtn setTitle:sharedManager.passingString forState:UIControlStateNormal];
            fiscalId = sharedManager.passingId;
            if ([fiscalId isEqualToString: @"00000000-0000-0000-0000-000000000000"]) {
                fiscalId = @"NA";
            }
        }
    }
}
@end
