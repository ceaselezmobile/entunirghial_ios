
#import "MenuViewController.h"
#import "ProfileTableViewCell.h"
#import "ViewController.h"
#import "UIView+Toast.h"
#import "SWRevealViewController.h"
#import "MySharedManager.h"
@implementation SWUITableViewCell

@end

@implementation MenuViewController
{
    long selectedSection;
    BOOL selectedSection1;
    BOOL selectedSection2;
    BOOL selectedSection3;
    BOOL selectedSection4;
    BOOL selectedSection5;
    BOOL selectedRow12;
    BOOL selectedRow13;
    BOOL selectedRow22;
    BOOL selectedRow30;
    BOOL selectedRow31;
    BOOL selectedRow32;
    BOOL selectedRow33;
    BOOL selectedRow40;
    BOOL TaskReports;
    BOOL MeetingTaskReports;
    MySharedManager *sharedManager;
    
    long selectedRow;
    IBOutlet UITableView *dataTable;
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 6;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (section == 1 && selectedSection1) {
        if (selectedRow12 && selectedRow13) {
            return 7;
        }
        else if (selectedRow12) {
            return 3;
        }
        else if (selectedRow13) {
            return 6;
        }
        return 2;
    }
    else if (section == 2 && selectedSection2) {
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"IsExternal"] isEqualToString:@"Yes"]) {
            
            if(selectedRow22){
                return 5;
            }
            return 2;
        }
        else if (selectedRow22) {
            return 7;
        }
        return 3;
    }
    else if (section == 3 && selectedSection3) {
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"IsExternal"] isEqualToString:@"Yes"]) {
            int rows = 0;
            if (selectedRow31) {
                rows += 1;
            }
            if (selectedRow32) {
                rows += 2;
            }
            if (selectedRow33) {
                rows += 1;
            }
            return 3+rows;
        }
        else{
            int rows = 0;
            if (selectedRow30) {
                rows += 3;
            }
            if (selectedRow31) {
                rows += 2;
            }
            if (selectedRow32) {
                rows += 4;
            }
            if (selectedRow33) {
                rows += 2;
            }
            if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"IsMOMApprover"] isEqualToString:@"YES"]) {
                rows += 1;
            }
            return 4+rows;
        }
        return 3;
    }
    //For DMS
    else if (section == 4 && selectedSection4) {
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"RoleDescription"] isEqualToString:@"Super Administrator"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"RoleDescription"] isEqualToString:@"Administrator"]) {
            return 4;
        }
        return 2;
    }
    else if (section == 5 && selectedSection5) {
        return 1;
    }
    
    return 0;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    sharedManager = [MySharedManager sharedManager];
    sharedManager.slideMenuSlected = @"yes";
    if (indexPath.section == 1 && indexPath.row == 1) {
        if (selectedRow12) {
            selectedRow12 = NO;
        }
        else
            selectedRow12 = YES;
    }
    if (selectedRow12) {
        if (indexPath.section == 1 && indexPath.row == 4) {
            if (selectedRow13) {
                selectedRow13 = NO;
            }
            else
                selectedRow13 = YES;
        }
    }
    else{
        if (indexPath.section == 1 && indexPath.row == 3) {
            if (selectedRow13) {
                selectedRow13 = NO;
            }
            else
                selectedRow13 = YES;
        }
    }
    
    if (indexPath.section == 2 && [[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"    • DashBoard"]) {
        if (selectedRow22) {
            selectedRow22 = NO;
        }
        else
            selectedRow22 = YES;
    }
    if (indexPath.section == 2 && [[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"    • Reports"]) {
        if (TaskReports) {
            TaskReports = NO;
        }
        else
            TaskReports = YES;
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"    • Adhoc Meeting"]) {
        if (selectedRow30) {
            selectedRow30 = NO;
        }
        else
            selectedRow30 = YES;
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"    • Meeting Actions"]) {
        if (selectedRow31) {
            selectedRow31 = NO;
        }
        else
            selectedRow31 = YES;
    }
    if (indexPath.section == 3 &&[[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"    • DashBoard"]) {
        if (selectedRow32) {
            selectedRow32 = NO;
        }
        else
            selectedRow32 = YES;
    }
    if (indexPath.section == 3 &&[[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"    • Document"]) {
        if (selectedRow33) {
            selectedRow33 = NO;
        }
        else
            selectedRow33 = YES;
    }
    if (indexPath.section == 3 && [[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"    • Reports"]) {
        if (MeetingTaskReports) {
            MeetingTaskReports = NO;
        }
        else
            MeetingTaskReports = YES;
    }
    if (indexPath.section == 4 && indexPath.row == 0) {
        //        if (selectedRow40) {
        //            selectedRow40 = NO;
        //        }
        //        else
        //            selectedRow40 = YES;
    }
    
    if (indexPath.section == 1 && indexPath.row == 0) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_UpdateStakeHolderViewController) {
            self.UpdateStakeHolderViewController = [storyBoard instantiateViewControllerWithIdentifier:@"UpdateStakeHolderViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_UpdateStakeHolderViewController];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if(selectedRow12){
        if (indexPath.section == 1 && indexPath.row == 2 ) {
            UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            if (!_ContactDirectoryViewController) {
                self.ContactDirectoryViewController = [storyBoard instantiateViewControllerWithIdentifier:@"ContactDirectoryViewController"];
            }
            UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_ContactDirectoryViewController];
            [self.revealViewController pushFrontViewController:nc animated:YES];
        }
        if (indexPath.section == 1 && indexPath.row == 5 ) {
            sharedManager.passingMode = @"Category wise companies";
            UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            if (!_DashBoardPieChartViewController) {
                self.DashBoardPieChartViewController = [storyBoard instantiateViewControllerWithIdentifier:@"DashBoardPieChartViewController"];
            }
            UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_DashBoardPieChartViewController];
            [self.revealViewController pushFrontViewController:nc animated:YES];
        }
        if (indexPath.section == 1 && indexPath.row == 6 )
        {
            sharedManager.passingMode = @"Industry wise companies";
            UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            if (!_DashBoardPieChartViewController) {
                self.DashBoardPieChartViewController = [storyBoard instantiateViewControllerWithIdentifier:@"DashBoardPieChartViewController"];
            }
            UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_DashBoardPieChartViewController];
            [self.revealViewController pushFrontViewController:nc animated:YES];
        }
    }
    else{
        if (indexPath.section == 1 && indexPath.row == 4 ) {
            sharedManager.passingMode = @"Category wise companies";
            UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            if (!_DashBoardPieChartViewController) {
                self.DashBoardPieChartViewController = [storyBoard instantiateViewControllerWithIdentifier:@"DashBoardPieChartViewController"];
            }
            UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_DashBoardPieChartViewController];
            [self.revealViewController pushFrontViewController:nc animated:YES];
        }
        if (indexPath.section == 1 && indexPath.row == 5 ) {
            sharedManager.passingMode = @"Industry wise companies";
            UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            if (!_DashBoardPieChartViewController) {
                self.DashBoardPieChartViewController = [storyBoard instantiateViewControllerWithIdentifier:@"DashBoardPieChartViewController"];
            }
            UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_DashBoardPieChartViewController];
            [self.revealViewController pushFrontViewController:nc animated:YES];
        }
    }
    
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"    • Add Action"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_ActionListViewController) {
            self.ActionListViewController = [storyBoard instantiateViewControllerWithIdentifier:@"ActionListViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_ActionListViewController];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"    • Update Action Progress"] || ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"           • Action Status"] && indexPath.section == 2 )) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_UpdateActionProgressViewController) {
            self.UpdateActionProgressViewController = [storyBoard instantiateViewControllerWithIdentifier:@"UpdateActionProgressViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_UpdateActionProgressViewController];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"           • Action Analysis(Self)"]) {
        sharedManager.passingMode = @"Action Analysis(Self)";
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_DashBoardPieChartViewController) {
            self.DashBoardPieChartViewController = [storyBoard instantiateViewControllerWithIdentifier:@"DashBoardPieChartViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_DashBoardPieChartViewController];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"           • Action Analysis(Others)"]) {
        sharedManager.passingMode = @"Action Analysis(Others)";
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_DashBoardPieChartViewController) {
            self.DashBoardPieChartViewController = [storyBoard instantiateViewControllerWithIdentifier:@"DashBoardPieChartViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_DashBoardPieChartViewController];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"           • Dependency Actions \n              Analysis"]) {
        sharedManager.passingMode = @"Dependency Actions Analysis";
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_DashBoardPieChartViewController) {
            self.DashBoardPieChartViewController = [storyBoard instantiateViewControllerWithIdentifier:@"DashBoardPieChartViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_DashBoardPieChartViewController];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"           • Delayed Completion  \n              Actions Analysis"]) {
        sharedManager.passingMode = @"Delayed Completion  Actions Analysis";
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_DashBoardPieChartViewController) {
            self.DashBoardPieChartViewController = [storyBoard instantiateViewControllerWithIdentifier:@"DashBoardPieChartViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_DashBoardPieChartViewController];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"          • Create Meeting"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_MeetingDetailsViewController) {
            self.MeetingDetailsViewController = [storyBoard instantiateViewControllerWithIdentifier:@"MeetingDetailsViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_MeetingDetailsViewController];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"           • Meeting Calendars"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_MeetingCalenderViewController) {
            self.MeetingCalenderViewController = [storyBoard instantiateViewControllerWithIdentifier:@"MeetingCalenderViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_MeetingCalenderViewController];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"           • Conduct Meeting"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_ConductMeetingViewController) {
            self.ConductMeetingViewController = [storyBoard instantiateViewControllerWithIdentifier:@"ConductMeetingViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_ConductMeetingViewController];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"           • Update Progress"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_UpdateMeetingStatusViewController) {
            self.UpdateMeetingStatusViewController = [storyBoard instantiateViewControllerWithIdentifier:@"UpdateMeetingStatusViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_UpdateMeetingStatusViewController];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"           • Approve/Disapprove Actions"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_ApproveActionsViewController) {
            self.ApproveActionsViewController = [storyBoard instantiateViewControllerWithIdentifier:@"ApproveActionsViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_ApproveActionsViewController];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"           • Meeting Action \n              Analysis(Self)" ]) {
        sharedManager.passingMode = @"Meeting Action Analysis(Self)";
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_DashBoardPieChartViewController) {
            self.DashBoardPieChartViewController = [storyBoard instantiateViewControllerWithIdentifier:@"DashBoardPieChartViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_DashBoardPieChartViewController];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"           • Meeting Action \n              Analysis(Others)"]) {
        sharedManager.passingMode = @"Meeting Action Analysis(Others)";
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_DashBoardPieChartViewController) {
            self.DashBoardPieChartViewController = [storyBoard instantiateViewControllerWithIdentifier:@"DashBoardPieChartViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_DashBoardPieChartViewController];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"           • Group Wise \n              Meetings"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_BarChartViewController) {
            self.BarChartViewController = [storyBoard instantiateViewControllerWithIdentifier:@"BarChartViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_BarChartViewController];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"           • My Meeting"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_MyMeetingViewController) {
            self.MyMeetingViewController = [storyBoard instantiateViewControllerWithIdentifier:@"MyMeetingViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_MyMeetingViewController];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"           • MOM/Document Retrieval"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_DocumentsRetrievalVC) {
            self.DocumentsRetrievalVC = [storyBoard instantiateViewControllerWithIdentifier:@"DocumentsRetrievalVC"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_DocumentsRetrievalVC];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"          • Document Upload"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_DocumentUploadVC) {
            self.DocumentUploadVC = [storyBoard instantiateViewControllerWithIdentifier:@"DocumentUploadVC"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_DocumentUploadVC];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"    • MOM Approval"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_MOMApprovalVC) {
            self.MOMApprovalVC = [storyBoard instantiateViewControllerWithIdentifier:@"MOMApprovalVC"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_MOMApprovalVC];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    //    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"    • Alert"]) {
    //        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //        if (!_AlertVC) {
    //            self.AlertVC = [storyBoard instantiateViewControllerWithIdentifier:@"AlertVC"];
    //        }
    //        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_AlertVC];
    //        [self.revealViewController pushFrontViewController:nc animated:YES];
    //    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"    • Invoice Approval"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_invoiceApprovalVC) {
            self.invoiceApprovalVC = [storyBoard instantiateViewControllerWithIdentifier:@"invoiceApprovalVC"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_invoiceApprovalVC];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"    • Invoice Closure Approval"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_InvoiceClosureApprovalVC) {
            self.InvoiceClosureApprovalVC = [storyBoard instantiateViewControllerWithIdentifier:@"InvoiceClosureApprovalVC"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_InvoiceClosureApprovalVC];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"    • Invoice Closure"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_InvoiceClosureAlertVC) {
            self.InvoiceClosureAlertVC = [storyBoard instantiateViewControllerWithIdentifier:@"InvoiceClosureAlertVC"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_InvoiceClosureAlertVC];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"    • Payment Receipt"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_PaymentRecieptListVC) {
            self.PaymentRecieptListVC = [storyBoard instantiateViewControllerWithIdentifier:@"PaymentRecieptListVC"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_PaymentRecieptListVC];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"    • Invoice creation"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_InvoiceCreatedListVC) {
            self.InvoiceCreatedListVC = [storyBoard instantiateViewControllerWithIdentifier:@"InvoiceCreatedListVC"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_InvoiceCreatedListVC];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"    • Invoice Dashboard"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_InvoiceDashboardVC) {
            self.InvoiceDashboardVC = [storyBoard instantiateViewControllerWithIdentifier:@"InvoiceDashboardVC"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_InvoiceDashboardVC];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"    • Invoices"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_InvoiceExternalVC) {
            self.InvoiceExternalVC = [storyBoard instantiateViewControllerWithIdentifier:@"InvoiceExternalVC"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_InvoiceExternalVC];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"    • Login Query"]) {
        
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_loginQueryVC) {
            
            self.loginQueryVC = [storyBoard instantiateViewControllerWithIdentifier:@"LoginQueryViewController"];
        }
        [self clearData];
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_loginQueryVC];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"    • Access Query"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_accessQueryVC) {
            self.accessQueryVC = [storyBoard instantiateViewControllerWithIdentifier:@"AccessQueryViewController"];
        }
        [self clearData];
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_accessQueryVC];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"    • Activity Query"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_activityQueryVC) {
            self.activityQueryVC = [storyBoard instantiateViewControllerWithIdentifier:@"ActivityQueryViewController"];
        }
        [self clearData];
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_activityQueryVC];
        [self.revealViewController pushFrontViewController:nc animated:YES];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"    • Access Group"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_accessGroupVC) {
            self.accessGroupVC = [storyBoard instantiateViewControllerWithIdentifier:@"AccessGroupTableViewController"];
        }
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_accessGroupVC];
        [self.revealViewController pushFrontViewController:nc animated:YES];
        // [self showLoader];
    }
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"    • My Access Query"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_myAccessQueryVC) {
            self.myAccessQueryVC = [storyBoard instantiateViewControllerWithIdentifier:@"MyAccessDataQueryViewController"];
        }
        
        [self clearData];
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_myAccessQueryVC];
        [self.revealViewController pushFrontViewController:nc animated:YES];
        // [self showLoader];
    }
    
    if ([[tableView cellForRowAtIndexPath:indexPath].textLabel.text isEqualToString:@"    • My Activity Query"]) {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        if (!_myActivityQueryVC) {
            self.myActivityQueryVC = [storyBoard instantiateViewControllerWithIdentifier:@"MyActivityDataQueryViewController"];
        }
        
        [self clearData];
        UINavigationController *nc = [[UINavigationController alloc]initWithRootViewController:_myActivityQueryVC];
        [self.revealViewController pushFrontViewController:nc animated:YES];
        // [self showLoader];
    }
    
    [dataTable reloadData];
}
-(void)logout {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self presentViewController:myNavController animated:YES completion:nil];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:nil forKey:@"EmailID"];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"subCell"];
    NSMutableArray *list = [[NSMutableArray alloc] init];
    if (indexPath.section == 1 && selectedSection1) {
        list =  [NSMutableArray arrayWithObjects:@"    • Add stakeholder",@"    • Data Query",nil];
        if (selectedRow12) {
            [list insertObject:@"           • Contact directory" atIndex:[list indexOfObject:@"    • Data Query"]+1];
        }
        
    }
    if (indexPath.section == 2 && selectedSection2 ) {
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"IsExternal"] isEqualToString:@"Yes"]) {
            if (selectedRow22) {
                list =  [NSMutableArray arrayWithObjects:@"    • Update Action Progress",@"    • DashBoard",@"           • Action Analysis(Self)",@"           • Dependency Actions \n              Analysis",@"           • Delayed Completion  \n              Actions Analysis",nil];
            }
            else{
                list =  [NSMutableArray arrayWithObjects:@"    • Update Action Progress",@"    • DashBoard",nil];
            }
            
        }
        else{
            if (selectedRow22) {
                list =  [NSMutableArray arrayWithObjects:@"    • Add Action",@"    • Update Action Progress",@"    • DashBoard",@"           • Action Analysis(Self)",@"           • Action Analysis(Others)",@"           • Dependency Actions \n              Analysis",@"           • Delayed Completion  \n              Actions Analysis",nil];
            }
            else{
                list =  [NSMutableArray arrayWithObjects:@"    • Add Action",@"    • Update Action Progress",@"    • DashBoard",nil];
            }
        }
    }
    if (indexPath.section == 3 && selectedSection3 ) {
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"IsExternal"] isEqualToString:@"Yes"]) {
            list =  [NSMutableArray arrayWithObjects:@"    • Meeting Actions",@"    • DashBoard",@"    • Document",nil];
            if (selectedRow31) {
                [list insertObject:@"           • Update Progress" atIndex:[list indexOfObject:@"    • Meeting Actions"]+1];
            }
            if (selectedRow32) {
                [list insertObject:@"           • Meeting Action \n              Analysis(Self)" atIndex:[list indexOfObject:@"    • DashBoard"]+1];
                [list insertObject:@"           • My Meeting" atIndex:[list indexOfObject:@"    • DashBoard"]+2];
            }
            if (selectedRow33) {
                [list insertObject:@"          • Document Upload" atIndex:[list indexOfObject:@"    • Document"]+1];
            }
            
        }
        else{
            list =  [NSMutableArray arrayWithObjects:@"    • Adhoc Meeting",@"    • Meeting Actions",@"    • DashBoard",@"    • Document",nil];
            
            if (selectedRow30) {
                [list insertObject:@"          • Create Meeting" atIndex:[list indexOfObject:@"    • Adhoc Meeting"]+1];
                [list insertObject:@"           • Meeting Calendars" atIndex:[list indexOfObject:@"    • Adhoc Meeting"]+2];
                [list insertObject:@"           • Conduct Meeting" atIndex:[list indexOfObject:@"    • Adhoc Meeting"]+3];
            }
            if (selectedRow31) {
                [list insertObject:@"           • Update Progress" atIndex:[list indexOfObject:@"    • Meeting Actions"]+1];
                [list insertObject:@"           • Approve/Disapprove Actions" atIndex:[list indexOfObject:@"    • Meeting Actions"]+2];
            }
            if (selectedRow32) {
                [list insertObject:@"           • Meeting Action \n              Analysis(Self)" atIndex:[list indexOfObject:@"    • DashBoard"]+1];
                [list insertObject:@"           • Meeting Action \n              Analysis(Others)" atIndex:[list indexOfObject:@"    • DashBoard"]+2];
                [list insertObject:@"           • Group Wise \n              Meetings" atIndex:[list indexOfObject:@"    • DashBoard"]+3];
                [list insertObject:@"           • My Meeting" atIndex:[list indexOfObject:@"    • DashBoard"]+4];
            }
            if (selectedRow33) {
                [list insertObject:@"          • Document Upload" atIndex:[list indexOfObject:@"    • Document"]+1];
                [list insertObject:@"           • MOM/Document Retrieval" atIndex:[list indexOfObject:@"    • Document"]+2];
            }
        }
        if ( [[[NSUserDefaults standardUserDefaults]objectForKey:@"IsMOMApprover"] isEqualToString:@"YES"]) {
            long indexValue = [list indexOfObject:@"    • DashBoard"];
            [list insertObject:@"    • MOM Approval" atIndex:indexValue];
        }
    }
    
    if (indexPath.section == 4 && selectedSection4 ) {
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"RoleDescription"] isEqualToString:@"Super Administrator"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"RoleDescription"] isEqualToString:@"Administrator"]) {
            list =  [NSMutableArray arrayWithObjects:@"    • Access Group",@"    • Login Query",@"    • Access Query",@"    • Activity Query",nil];
        }
        else{
            list =  [NSMutableArray arrayWithObjects:@"    • My Access Query",@"    • My Activity Query",nil];
        }
    }
    
    /* if (indexPath.section == 4 && selectedSection4 ) {
     if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"IsExternal"] isEqualToString:@"Yes"]) {
     list =  [NSMutableArray arrayWithObjects:@"    • Invoices",nil];
     }
     else{
     list =  [NSMutableArray arrayWithObjects:@"    • Invoice creation",@"    • Invoice Approval",@"    • Payment Receipt",@"    • Invoice Closure",@"    • Invoice Closure Approval",@"    • Invoice Dashboard",nil];
     }
     }*/
    
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.adjustsFontSizeToFitWidth = NO;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.textLabel.text = [list objectAtIndex:indexPath.row];
    return cell;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    ProfileTableViewCell *cell;
    if (section == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"profile"];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        cell.nameLabel.text =[defaults objectForKey:@"EmployeeName"];
        cell.mailIDLabel.text =[defaults objectForKey:@"EmailID"];
    }
    else{
        cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        NSArray *imageList = [[NSArray alloc] initWithObjects:@"",@"StakeHolders.png",@"Action Planner.png",@"Meeting.png",@"Documents.png",@"logout.png",nil];
        NSArray *list = [[NSArray alloc] initWithObjects:@"",@"External Stakeholder Management",@"Action Management",@"Meeting Management",@"Document Management",@"Logout",nil];
        cell.headerTittleLabel.text = [list objectAtIndex:section];
        cell.headerImageView.image = [UIImage imageNamed:[imageList objectAtIndex:section]];
        cell.clickableBtn.tag = section;
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if (section == 0) {
        return 180;
    }
    if ([[defaults objectForKey:@"AccessModule"] rangeOfString:@"External Stakeholder Management"].location == NSNotFound) {
        if (section == 1) {
            return 0;
        }
    }
    if ([[defaults objectForKey:@"AccessModule"] rangeOfString:@"Meeting Management"].location == NSNotFound) {
        if (section == 3) {
            return 0;
        }
    }
    if ([[defaults objectForKey:@"AccessModule"] rangeOfString:@"Action Management"].location == NSNotFound) {
        if (section == 2) {
            return 0;
        }
    }
    if ([[defaults objectForKey:@"AccessModule"] rangeOfString:@"Document Management"].location == NSNotFound) {
        if (section == 4) {
            return 0;
        }
    }
    
    return 70;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
- (IBAction)sectionBtn:(id)sender {
    UIButton *clickedButton = (UIButton*)sender;
    
    if (clickedButton.tag == 1 && ![[[NSUserDefaults standardUserDefaults] objectForKey:@"IsExternal"] isEqualToString:@"Yes"]) {
        
        if (selectedSection1) {
            selectedSection1 = NO;
        }
        else
            selectedSection1 =YES;
    }
    if (clickedButton.tag == 2) {
        if (selectedSection2) {
            selectedSection2 = NO;
        }
        else
            selectedSection2 = YES;
    }
    if (clickedButton.tag == 3) {
        if (selectedSection3) {
            selectedSection3 = NO;
        }
        else
            selectedSection3 = YES;
    }
    if (clickedButton.tag == 4) {
        if (selectedSection4) {
            selectedSection4 = NO;
        }
        else
            selectedSection4 = YES;
    }
    if (clickedButton.tag == 5) {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Logout"
                                                                      message:@"Are you sure?"
                                                               preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"Yes"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action)
                                    {
                                        [self logout];
                                    }];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:nil];
        
        [alert addAction:yesButton];
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    if (selectedSection == clickedButton.tag) {
        selectedRow = 0;
        if (clickedButton.tag == 1) {
            selectedRow12 = NO;
            selectedRow13 = NO;
        }
        if (clickedButton.tag == 2) {
            selectedRow22 = NO;
            TaskReports = NO;
        }
        
        if (clickedButton.tag == 3) {
            selectedRow30 = NO;
            selectedRow31 = NO;
            selectedRow32 = NO;
            selectedRow33 = NO;
        }
    }
    selectedSection = clickedButton.tag;
    [dataTable reloadData];
}
- (IBAction)changePassword:(id)sender {
    NSLog(@"clicked=-=====");
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    if (!_SettingsVC) {
        self.SettingsVC = [storyBoard instantiateViewControllerWithIdentifier:@"SettingsVC"];
    }
    [self presentViewController:_SettingsVC animated:YES completion:nil];
}

-(void)showLoader
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    
    hud.mode = MBProgressHUDModeText;
    hud.label.text = @"Work InProgress...";
    hud.label.font=[UIFont fontWithName:@"Avenir-Medium" size:14];
    hud.margin = 10.f;
    [hud setOffset:CGPointMake(0, self.view.frame.size.height/2)];
    hud.removeFromSuperViewOnHide = YES;
    [hud hideAnimated:YES afterDelay:0.5];
}

-(void) clearData
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"clear_data" object:self];
}


@end

