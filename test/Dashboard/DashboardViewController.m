//
//  DashboardViewController.m
//  test
//
//  Created by ceaselez on 27/11/17.
//  Copyright © 2017 ceaselez. All rights reserved.
//

#import "DashboardViewController.h"



@interface DashboardViewController ()
@property (weak, nonatomic) IBOutlet UIButton *revealButtonItem;
@property (nonatomic, strong) DashBoardPieChartViewController *DashBoardPieChartViewController;

@end

@implementation DashboardViewController
{
    MySharedManager *sharedManager;
}
- (void)viewDidLoad {
    sharedManager = [MySharedManager sharedManager];
    sharedManager.slideMenuSlected = @"Dashboard";
    
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    [self.revealViewController revealToggleAnimated:YES];
    
    if ( revealViewController )
    {
        [self.revealButtonItem addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
}
- (IBAction)stackholderBtn:(id)sender {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"AccessModule"] rangeOfString:@"External Stakeholder Management"].location == NSNotFound) {
        [self showAlertViewWithMessage:@"You do not have access to this module" withTitle:@""];
    }
    else{
        sharedManager.passingMode = @"Category wise companies";
        sharedManager.slideMenuSlected = @"Dashboard";
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        DashBoardPieChartViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"DashBoardPieChartViewController"];
        [self presentViewController:myNavController animated:YES completion:nil];
    }
}

- (IBAction)actionplannerBtn:(id)sender {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"AccessModule"] rangeOfString:@"Action Management"].location == NSNotFound) {
        [self showAlertViewWithMessage:@"You do not have access to this module" withTitle:@""];
    }
    else{
        sharedManager.passingMode = @"Action Analysis(Self)";
        sharedManager.slideMenuSlected = @"Dashboard";
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        DashBoardPieChartViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"DashBoardPieChartViewController"];
        [self presentViewController:myNavController animated:YES completion:nil];
        
    }
}
- (IBAction)meetingManagementBtn:(id)sender {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"AccessModule"] rangeOfString:@"Meeting Management"].location == NSNotFound) {
        [self showAlertViewWithMessage:@"You do not have access to this module" withTitle:@""];
    }
    else{
        sharedManager.passingMode = @"Meeting Action Analysis(Self)";
        sharedManager.slideMenuSlected = @"Dashboard";
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        DashBoardPieChartViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"DashBoardPieChartViewController"];
        [self presentViewController:myNavController animated:YES completion:nil];
    }
    
}

- (IBAction)documentManagementAction:(id)sender {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"AccessModule"] rangeOfString:@"Document Management"].location == NSNotFound) {
        [self showAlertViewWithMessage:@"You do not have access to this module" withTitle:@""];
    }
    else{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        AccessGroupTableViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"AccessGroupTableViewController"];
        [self presentViewController:myNavController animated:YES completion:nil];
    }
    
}


-(void) showAlertViewWithMessage:(NSString *)message withTitle:(NSString *)title  {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert] ;
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action){
                             [alert dismissViewControllerAnimated:YES completion:nil];
                         }];
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
