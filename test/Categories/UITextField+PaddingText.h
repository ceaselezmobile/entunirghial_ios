//
//  UITextField+PaddingText.h
//  test
//
//  Created by ceazeles on 02/01/19.
//  Copyright © 2019 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITextField (PaddingText)

-(void) setLeftPadding:(int) paddingValue;

-(void) setRightPadding:(int) paddingValue;

@end

NS_ASSUME_NONNULL_END
