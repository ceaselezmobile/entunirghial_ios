//
//  AccessGroupTableViewController.m
//  test
//
//  Created by ceazeles on 27/12/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "AccessGroupTableViewController.h"

@interface AccessGroupTableViewController ()

@end

@implementation AccessGroupTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    searchResultArray = [NSMutableArray array];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    noDataLabel = [[UILabel alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height/2,self.view.frame.size.width ,50)];
    
    if ( revealViewController )
    {
        [self.menuButton setImage:[UIImage imageNamed:@"Menu.png"]  forState:UIControlStateNormal];
        [self.menuButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    else{
        [self.menuButton setImage:[UIImage imageNamed:@"BackBtn.png"] forState:UIControlStateNormal];
        [self.menuButton addTarget:self.revealViewController action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    }
    
   // [self getDelegate:[NSString stringWithFormat:@"/GetAccessGroupMembers?userid=%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"UserID"]]];
    
    [self.searchBar setValue:[UIColor whiteColor] forKeyPath:@"_searchField._placeholderLabel.textColor"];
    
    refreshControl = [[UIRefreshControl alloc]init];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    if (@available(iOS 10.0, *)) {
        self.tableView.refreshControl = refreshControl;
    } else {
        [self.tableView addSubview:refreshControl];
    }
   
}

-(void)dismiss
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self getDelegate:[NSString stringWithFormat:@"/GetAccessGroupMembers?userid=%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"UserID"]]];
    self.navigationController.navigationBar.hidden = YES;
}

-(void)refreshTable{
   [self getDelegate:[NSString stringWithFormat:@"/GetAccessGroupMembers?userid=%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"UserID"]]];
    [refreshControl endRefreshing];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section == 1)
    {
        if(self.searchBarActive)
        {
            return searchResultArray.count;
        }
        else
        {
            return array.count;
        }
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AccessGroupTableViewCell *cell;
    NSArray *arr = [NSArray array];
    if(self.searchBarActive)
    {
        arr = [searchResultArray mutableCopy];
    }
    else
    {
        arr = [array mutableCopy];
    }
    
    if (indexPath.section == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"headerCell" forIndexPath:indexPath];
        
        cell.groupNameHeaderText.text = @"Group Name";
        cell.stakeHolderTypeHeaderText.text = @"Stakeholder Type";
        cell.nameHeaderText.text = @"Stakeholder Name";
    }
    if (indexPath.section == 1) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"dataCell" forIndexPath:indexPath];
        if(array.count > 0)
        {
            
            NSDictionary *dict = arr[indexPath.row];
            cell.groupNameDataText.text = dict[@"GroupName"];
            cell.stakeHolderTypeText.text = dict[@"Type"];
            cell.nameDataText.text = dict[@"StakeHolderName"];
        }
    }
    return cell;
}

-(void)getDelegate :(NSString *)url
{
    if([Utitlity isConnectedTointernet]){
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[ServiceManager sharedInstance] getWithParameter:nil withUrl:[NSString stringWithFormat:@"%@",url] withHandler:^(id responseObject, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            self -> array = [NSMutableArray array];
            [self->array addObjectsFromArray:responseObject];
            if(self->array.count > 0)
            {
                [self removeNoDataFound:self.view];
                [self.tableView reloadData];
            }
            else{
                [self showNoDataFound:self.view];
            }
            
            NSLog(@"Search Response  %@",responseObject);
        });
    }];
    }
    else{
        [[Utitlity sharedInstance] showAlertViewWithMessage:NOInternetMessage withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    [searchResultArray removeAllObjects];
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"GroupName contains[c] %@", searchText];
    
    searchResultArray  = [NSMutableArray arrayWithArray:[array filteredArrayUsingPredicate:resultPredicate]];
    if(searchResultArray.count > 0)
    {
        self.tableView.hidden = NO;
        [self.tableView reloadData];
        [self removeNoDataFound:self.view];
    }
    else{
        self.tableView.hidden = YES;
        [self showNoDataFound:self.view];
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchText.length>0)
    {
        self.searchBarActive = YES;
        [self filterContentForSearchText:searchText scope:[[self.searchBar scopeButtonTitles] objectAtIndex:[self.searchBar selectedScopeButtonIndex]]];
        
    }else{
        
        [self removeNoDataFound:self.view];
        self.searchBarActive = NO;
        self.tableView.hidden = NO;
        [self.tableView reloadData];
    }
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self cancelSearching];
    [self.tableView reloadData];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    self.searchBarActive = YES;
    [self.view endEditing:YES];
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [self.searchBar setShowsCancelButton:NO animated:YES];
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    self.searchBarActive = NO;
    [self.searchBar setShowsCancelButton:NO animated:YES];
}
-(void)cancelSearching
{
    self.searchBarActive = NO;
    [self.searchBar resignFirstResponder];
    self.searchBar.text  = @"";
}


-(void)showNoDataFound :(UIView *)view
{
    if(![noDataLabel isDescendantOfView:view])
    {
        [view addSubview:noDataLabel];
    }
    [noDataLabel setFont:[UIFont systemFontOfSize:20]];
    noDataLabel.textAlignment=NSTextAlignmentCenter;
    noDataLabel.layer.masksToBounds  = YES;
    noDataLabel.layer.shadowOpacity  = 2.5;
    noDataLabel.layer.shadowColor    = [[UIColor grayColor] CGColor];
    noDataLabel.layer.shadowOffset   = CGSizeMake(0, 1);
    noDataLabel.layer.shadowRadius   = 2;
    noDataLabel.text=@"No Data Found";
}

-(void)removeNoDataFound :(UIView *)view
{
    [noDataLabel removeFromSuperview];
}


@end
