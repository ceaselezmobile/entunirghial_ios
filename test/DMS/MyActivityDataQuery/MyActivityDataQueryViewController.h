//
//  MyActivityDataQueryViewController.h
//  test
//
//  Created by ceazeles on 09/01/19.
//  Copyright © 2019 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyActivityDataQueryTableViewCell.h"
#import "SWRevealViewController.h"
#import "THDatePickerViewController.h"
#import "Utitlity.h"
#import "MyAccessQueryTableViewController.h"
#import "UITextField+PaddingText.h"
#import "MyActivityQueryTableViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyActivityDataQueryViewController : UIViewController
{
    THDatePickerViewController * datePicker;
    NSDateFormatter * formatter;
    NSString *dateType;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *menuButton;

@end

NS_ASSUME_NONNULL_END
