//
//  LoginQueryTableViewController.m
//  test
//
//  Created by ceazeles on 24/12/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "LoginQueryTableViewController.h"

@interface LoginQueryTableViewController ()

@end

@implementation LoginQueryTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    noDataLabel = [[UILabel alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height/2,self.view.frame.size.width ,50)];
    
    
    [self getDelegate:_theURL];
    
    refreshControl = [[UIRefreshControl alloc]init];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    if (@available(iOS 10.0, *)) {
        self.tableView.refreshControl = refreshControl;
    } else {
        [self.tableView addSubview:refreshControl];
    }
}

-(void)refreshTable{
    [self getDelegate:self.theURL];
    [refreshControl endRefreshing];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section == 1)
    {
        return array.count;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LoginDataQueryTableViewCell *cell;
    
    if (indexPath.section == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"headerCell" forIndexPath:indexPath];
        
        cell.loginDateHeaderText.text = @"Login Date/Time";
        cell.loginIpHeaderText.text = @"Login IP Address";
        cell.locationHeaderText.text = @"Location";
    }
    if (indexPath.section == 1) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"dataCell" forIndexPath:indexPath];
        if(array.count > 0)
        {
        
           NSDictionary *dict = array[indexPath.row];
            cell.loginDateDataText.text = dict[@"LogInTime"];
            cell.loginIpDataText.text = dict[@"LogInIP"];
            cell.locationDataText.text = dict[@"LogInCity"];
        }
    }
    return cell;
}

-(void)getDelegate :(NSString *)url
{
    if([Utitlity isConnectedTointernet]){
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[ServiceManager sharedInstance] getWithParameter:nil withUrl:[NSString stringWithFormat:@"%@",url] withHandler:^(id responseObject, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:self.view animated:YES];

            array = [NSMutableArray array];
            [self->array addObjectsFromArray:responseObject];
            if(self->array.count > 0)
            {
                [self removeNoDataFound:self.view];
                [self.tableView reloadData];
            }
            else{
                [self showNoDataFound:self.view];
            }
            
            NSLog(@"Search Response  %@",responseObject);
        });
    }];
    }
    else{
        [[Utitlity sharedInstance] showAlertViewWithMessage:NOInternetMessage withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
             {
                 
             }];
            
       
    }
}

-(void)showNoDataFound :(UIView *)view
{
    if(![noDataLabel isDescendantOfView:view])
    {
        [view addSubview:noDataLabel];
    }
    [noDataLabel setFont:[UIFont systemFontOfSize:20]];
    noDataLabel.textAlignment=NSTextAlignmentCenter;
    noDataLabel.layer.masksToBounds  = YES;
    noDataLabel.layer.shadowOpacity  = 2.5;
    noDataLabel.layer.shadowColor    = [[UIColor grayColor] CGColor];
    noDataLabel.layer.shadowOffset   = CGSizeMake(0, 1);
    noDataLabel.layer.shadowRadius   = 2;
    noDataLabel.text=@"No Data Found";
}

-(void)removeNoDataFound :(UIView *)view
{
    [noDataLabel removeFromSuperview];
}

- (IBAction)backButtonClick:(id)sender {
    
     
     [self.navigationController popViewControllerAnimated:YES];
}

@end
