//
//  MyAccessDataQueryViewController.h
//  test
//
//  Created by ceazeles on 08/01/19.
//  Copyright © 2019 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyAccessDataQueryTableViewCell.h"
#import "SWRevealViewController.h"
#import "THDatePickerViewController.h"
#import "Utitlity.h"
#import "MyAccessQueryTableViewController.h"
#import "UITextField+PaddingText.h"
#import "MyAccessQueryTableViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyAccessDataQueryViewController : UIViewController
{
    THDatePickerViewController * datePicker;
    NSDateFormatter * formatter;
    NSString *dateType;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *menuButton;

@end

NS_ASSUME_NONNULL_END
