//
//  PreviewMOMViewController.h
//  test
//
//  Created by ceaselez on 24/01/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PreviewMOMViewController : UIViewController
@property(nonatomic,strong) NSString *pdfUrl;
@property(nonatomic,strong) NSString *ActualStartTime;
@property(nonatomic,strong) NSString *ActualEndTime;
@property(nonatomic,strong) NSDictionary *meetingDetails;
@end
