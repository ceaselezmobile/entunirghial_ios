//
//  MOMMemberTableViewCell.h
//  test
//
//  Created by ceaselez on 24/01/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MOMMemberTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *tittleLabel;
@property (weak, nonatomic) IBOutlet UITextField *contentTF;
@property (weak, nonatomic) IBOutlet UIButton *cellBtn;

@end
